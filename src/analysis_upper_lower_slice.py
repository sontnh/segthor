import os
import csv
import glob
import SimpleITK as sitk
import numpy as np

DIR_SRC = '\\\\data5\\ImageData\\MachineLearning\\Challenge_SegTHOR\\train_mha_resampled'
DIR_DST = '\\\\data5\\ImageData\\MachineLearning\\Challenge_SegTHOR'


def main():
    start_end_slice_list = []
    for pid in range(1, 41):
        print('pid: %d' % pid)
        fn_gt = os.path.join(DIR_SRC, 'Patient_%02d_GT.mha' % pid)

        img_gt = sitk.ReadImage(fn_gt)

        raw_gt = sitk.GetArrayFromImage(img_gt)

        gt_shape = np.shape(raw_gt)

        start_end_slice = []

        for label in range(1, 5):

            binary_label = np.zeros(gt_shape).astype(np.uint8)
            binary_label[np.where(raw_gt == label)] = 1

            start_slice = 0
            for zz in range(0, gt_shape[0]):
                tmp_slice = binary_label[zz, :, :]
                tmp_sum = np.sum(tmp_slice)
                if tmp_sum > 0:
                    start_slice = zz
                    break

            end_slice = gt_shape[0] - 1
            for zz in reversed(range(0, gt_shape[0])):
                tmp_slice = binary_label[zz, :, :]
                tmp_sum = np.sum(tmp_slice)
                if tmp_sum > 0:
                    end_slice = zz
                    break
            start_end_slice += [start_slice, end_slice]

        start_end_slice_list.append(start_end_slice)

    fn_output_csv = os.path.join(DIR_DST, 'start_end_slice.csv')
    with open(fn_output_csv, 'w', newline='') as fid:
        writer = csv.writer(fid)
        writer.writerow(
            ['Eso_start', 'Eso_end', 'Heart_start', 'Heart_end', 'Trachea_start', 'Trachea_end', 'Aorta_start',
             'Aorta_end'])
        for start_end_slice in start_end_slice_list:
            writer.writerow(start_end_slice)


if __name__ == '__main__':
    main()
