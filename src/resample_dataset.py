import os
import SimpleITK as sitk
import glob

DIR_SRC = '\\\\data4\\ImageData\\SegTHOR_challenge\\train'
DIR_MHA_DST = '\\\\data5\\ImageData\\MachineLearning\\Challenge_SegTHOR\\train_mha'
DIR_MHA_RESAMPLE_DST = '\\\\data5\\ImageData\\MachineLearning\\Challenge_SegTHOR\\train_mha_resampled'

target_spacing = (0.9, 0.9, 2.0)


def nii2mha_and_resample():
    dir_src_list = glob.glob(os.path.join(DIR_SRC, '*'))
    for dir_src in dir_src_list:
        if not os.path.isdir(dir_src): continue
        pat_id = dir_src.split(os.sep)[-1]
        fn_list = glob.glob(os.path.join(dir_src, '*.nii.gz'))
        for fn in fn_list:
            print('fn: %s' % fn)
            img = sitk.ReadImage(fn)
            src_spacing = img.GetSpacing()
            src_dim = img.GetSize()
            resampler = sitk.ResampleImageFilter()
            resampler.SetReferenceImage(img)
            resampler.SetOutputSpacing(target_spacing)

            if 'GT' in fn:
                fn_mha = os.path.join(DIR_MHA_DST, pat_id + '_GT.mha')
                fn_mha_resample = os.path.join(DIR_MHA_RESAMPLE_DST, pat_id + '_GT.mha')
                resampler.SetInterpolator(sitk.sitkNearestNeighbor)
            else:
                fn_mha = os.path.join(DIR_MHA_DST, pat_id + '.mha')
                fn_mha_resample = os.path.join(DIR_MHA_RESAMPLE_DST, pat_id + '.mha')
                resampler.SetInterpolator(sitk.sitkBSpline)

            output_size = (int((src_dim[0] * src_spacing[0] / target_spacing[0] // 4) * 4),
                           int((src_dim[1] * src_spacing[1] / target_spacing[1] // 4) * 4),
                           int((src_dim[2] * src_spacing[2] / target_spacing[2] // 4) * 4))
            resampler.SetSize(output_size)
            resampled_img_mha = resampler.Execute(img)
            sitk.WriteImage(img, fn_mha, True)  # original spacing
            sitk.WriteImage(resampled_img_mha, fn_mha_resample, True)  # resampled for target spacing


if __name__ == '__main__':
    nii2mha_and_resample()
