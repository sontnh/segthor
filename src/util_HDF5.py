import h5py
import os
# from mylogger import logger
import numpy as np
import re


class HDF5Common:
    def __init__(self):
        self.h5file = None
        self.dataset_list = []
        self.num_dataset_list = 0

    def update_dataset_list(self):
        def visitor_func(name, node):
            if isinstance(node, h5py.Dataset):
                # node is a dataset
                self.dataset_list.append(name)
                # else:
                # node is a group

        self.h5file.visititems(visitor_func)
        self.num_dataset_list = len(self.dataset_list)


class HDF5Reader(HDF5Common):
    def __init__(self, fn):
        super(HDF5Reader, self).__init__()
        self.h5file = h5py.File(fn)
        self.update_dataset_list()
        print(self.dataset_list)
        print(self.num_dataset_list)

    def __del__(self):
        # self.h5file.close()
        pass

    def search_dataset_via_regex(self, pattern, is_sorted=True):

        ret = []
        for dataset_name in self.dataset_list:
            if re.search(pattern, dataset_name):
                ret.append(dataset_name)

        if is_sorted:
            ret = sorted(ret)

        return ret


class HDF5Writer(HDF5Common):
    def __init__(self, fn):
        super(HDF5Writer, self).__init__()
        is_exist = os.path.isfile(fn)
        if is_exist:
            self.h5file = h5py.File(fn, 'r+')
        else:
            self.h5file = h5py.File(fn, 'a')

        self.update_dataset_list()

    def __del__(self):
        self.h5file.close()
        # pass

    def create_dataset_to_hdf5(self, path, data, spacing_in_mm=None, compression=False):
        # creating
        if compression:
            dset = self.h5file.create_dataset(path, data=data, compression='gzip')
        else:
            dset = self.h5file.create_dataset(path, data=data)

        if spacing_in_mm is not None:
            dset.attrs['element_size_um'] = [x * 1000 for x in spacing_in_mm]

    def overwrite_dataset_to_hdf5(self, path, data, spacing_in_mm=None, compression=False):
        self.write_dataset_to_hdf5(path, data, spacing_in_mm, compression)

    def write_dataset_to_hdf5(self, path, data, spacing_in_mm=None, compression=False):

        # if dataset exist on the path, then delete it and write (for overwriting)
        is_exist = False
        for i in range(self.num_dataset_list):
            if path in self.dataset_list[i]:
                is_exist = True
            if is_exist: break

        if is_exist:
            print('Dataset (%s) exist. It will be deleted for replacement.' % path)
            del self.h5file[path]

        # writing
        self.create_dataset_to_hdf5(path, data, spacing_in_mm, compression)
        # self.update_dataset_list()


def load_raw(fn, dims, dtype):
    array_tmp = np.fromfile(fn, dtype, np.product(dims)).reshape(dims)
    array_tmp = array_tmp.astype(dtype)
    return array_tmp
