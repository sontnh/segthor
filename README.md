## SegTHOR: SEgmentation of THoracic Organs at Risk in CT images
https://segthor.grand-challenge.org/

### Training Data
//Data4/ImageData/SegTHOR_challenge

### Files

run ./training.cmd to train the model  
./src/config.xml            define parameters for training  

./src/Model_SegthorNet.py   Define DenseNet Model  
./src/run_train.py          for training  
./src/run.eval.py           for testing  
./src/train_SegthorNet.py   define utility functions calling DenseNet model from Model_SegthorNet.py for run_train.py  
./src/util_args.py          define config argument utility function  
./src/util_HDF5.py          wraper IO utility of HDF5 database  
./src/misc.py               define augument functions  
./src/eval_vis.py           visual the test results  
./src/mylogger.py           logging stuff  
./src/AdamAccumulate.py     Adam Optimizer decoupe decay regularization  
./src/analysis_upper_lower_slice.py     SegThor dataset analysis  
./src/analyze_dataset_ratio.py          SegThor analyze the balance of dataset  
./src/analysis_upper_lower_slice.py     SegThor dataset analysis  
./src/generate_training_dataset.py      Generating the patch in 3D and 2.5D 