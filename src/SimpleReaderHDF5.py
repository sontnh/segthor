import time
import random
import numpy as np
from src.mylogger import training_logger
import re
import h5py
from pathlib import Path
import os

import csv
from scipy.ndimage import zoom
from src.misc import rotations16_onlyz
import SimpleITK as sitk

random.seed(0)


class HDF5Common:
    def __init__(self):
        self.h5file = None
        self.dataset_list = []
        self.num_dataset_list = 0

    def update_dataset_list(self):
        def visitor_func(name, node):
            if isinstance(node, h5py.Dataset):
                # node is a dataset
                self.dataset_list.append(name)
                # else:
                # node is a group

        self.h5file.visititems(visitor_func)
        self.num_dataset_list = len(self.dataset_list)


class HDF5Reader(HDF5Common):
    def __init__(self, fn):
        super(HDF5Reader, self).__init__()
        self.h5file = h5py.File(fn)
        fn_data_list = os.path.join(str(Path(fn).parent), str(Path(fn).stem) + '.h5csv')
        if os.path.isfile(fn_data_list):
            self.dataset_list = self.load_dataset_list_csv(fn_data_list)
        else:
            self.update_dataset_list()
            self.save_dataset_list_csv(fn_data_list)
        # print(self.dataset_list)
        # print(self.num_dataset_list)

    def __del__(self):
        # self.h5file.close()
        pass

    def load_dataset_list_csv(self, fn):
        data_list = []
        with open(fn, 'r') as fid:
            rdr = csv.reader(fid)
            for line in rdr:
                data_list.append(line[0])

        return data_list

    def save_dataset_list_csv(self, fn):
        with open(fn, 'w', newline='') as fid:
            wrt = csv.writer(fid)
            for data in self.dataset_list:
                wrt.writerow([data])

    def search_dataset_via_regex(self, pattern, is_sorted=True):

        # ret = []
        # for dataset_name in self.dataset_list:
        #     if re.search(pattern, dataset_name):
        #         ret.append(dataset_name)
        r = re.compile(pattern=pattern)
        ret = list(filter(r.search, self.dataset_list))

        if is_sorted:
            ret = sorted(ret)

        return ret


# target_dims = (144, 480, 480)
class SimpleReaderHDF5:
    def __init__(self, fn_hdf5_list, target_dims, input_pattern_regex='dcm', label_pattern_regex='label',
                 random_shuffle=True, intensity_norm=True, norm_wind_lower=-1200, norm_wind_upper=2000,
                 swap_channel=False, augmentation_onthefly_active=True):

        self.random_shuffle = random_shuffle

        self.augmentation_active = augmentation_onthefly_active
        self.random_crop_range = 6
        self.random_scale_range = 0.2
        self.random_rotflip_range = 16
        self.random_intensity_offset_range = 10  # HU unit

        self.target_dims = target_dims

        self.hdf5_reader_list = []
        self.data_list = []
        self.swap_channel = swap_channel
        for idx_hdf5, fn_hdf5 in enumerate(fn_hdf5_list):
            training_logger.info('Reading %s file....' % fn_hdf5)
            hdf5_reader_tmp = HDF5Reader(fn_hdf5)
            input_list_tmp = hdf5_reader_tmp.search_dataset_via_regex(input_pattern_regex, is_sorted=True)
            label_list_tmp = hdf5_reader_tmp.search_dataset_via_regex(label_pattern_regex, is_sorted=True)
            for i in range(len(input_list_tmp)):
                self.data_list.append([input_list_tmp[i], label_list_tmp[i], idx_hdf5])

            self.hdf5_reader_list.append(hdf5_reader_tmp)

        self.random_shuffle = random_shuffle

        self.list_size = self.get_list_size()
        self.random_shuffle_table = np.arange(0, self.list_size)
        if random_shuffle:
            random.shuffle(self.random_shuffle_table)
        self.idx_minibatch = 0
        self.intensity_norm = intensity_norm
        self.norm_wind_lower = norm_wind_lower
        self.norm_wind_upper = norm_wind_upper

        training_logger.info('list_size: %d' % self.list_size)

    def normalize_intensity(self, vol):
        slope = 1 / (self.norm_wind_upper - self.norm_wind_lower)
        vol = vol - self.norm_wind_lower
        vol = vol * slope
        vol[np.where(vol < 0)] = 0
        vol[np.where(vol > 1)] = 1

        return vol

    def update_list_size(self):
        self.list_size = len(self.data_list)

    def get_list_size(self):
        self.update_list_size()
        return self.list_size

    def next_randminibatch(self, num_img, idx_rand):
        input_img_list = []
        label_img_list = []

        idx_rand = idx_rand % self.list_size
        tmp_idx_minibatch = idx_rand
        for n in range(num_img):

            idx = self.random_shuffle_table[tmp_idx_minibatch]
            fn_input = self.data_list[idx][0]
            fn_label = self.data_list[idx][1]
            hdf5_idx = self.data_list[idx][2]
            h5_reader = self.hdf5_reader_list[hdf5_idx]

            input_img = np.array(h5_reader.h5file[fn_input]).astype(np.float32)
            label_img = np.array(h5_reader.h5file[fn_label]).astype(np.float32)

            if self.augmentation_active:
                input_img, label_img = self.augmentation_on_the_fly(input_img, label_img)
            else:
                crop_cntr_y, crop_cntr_x = (int(np.shape(input_img)[1] / 2), int(np.shape(input_img)[2] / 2))
                input_img = self.crop_volume(input_img, crop_cntr_y, crop_cntr_x)
                label_img = np.squeeze(self.crop_volume(np.expand_dims(label_img, axis=0), crop_cntr_y, crop_cntr_x))

            if self.intensity_norm:
                input_img = self.normalize_intensity(input_img)

            label_img_channelized_shape = tuple([5] + list(np.shape(label_img)))
            label_img_channelized = np.zeros(shape=label_img_channelized_shape).astype(np.float32)

            for i in range(5):
                tmp = np.zeros(shape=np.shape(label_img))
                tmp[np.where(label_img == i)] = 1
                label_img_channelized[i, :, :] = tmp

            if self.swap_channel:
                input_img = self.swap_channel_order(input_img)
                label_img_channelized = self.swap_channel_order(label_img_channelized)

            input_img_list.append(input_img)
            label_img_list.append(label_img_channelized)

            tmp_idx_minibatch += 1
            if tmp_idx_minibatch >= self.list_size:
                tmp_idx_minibatch = 0

        return np.array(input_img_list), np.array(label_img_list)

    def at(self, idx):
        if idx > self.list_size - 1:
            idx = idx % self.list_size

        input_img_list = []
        label_img_list = []

        fn_input = self.data_list[idx][0]
        fn_label = self.data_list[idx][1]
        hdf5_idx = self.data_list[idx][2]
        h5_reader = self.hdf5_reader_list[hdf5_idx]

        input_img = np.array(h5_reader.h5file[fn_input]).astype(np.float32)
        label_img = np.array(h5_reader.h5file[fn_label]).astype(np.float32)

        if self.augmentation_active:
            input_img, label_img = self.augmentation_on_the_fly(input_img, label_img)
        else:
            crop_cntr_z, crop_cntr_y, crop_cntr_x = (
                int(np.shape(input_img)[0] / 2), int(np.shape(input_img)[1] / 2), int(np.shape(input_img)[2] / 2))
            input_img = self.crop_volume(input_img, crop_cntr_z, crop_cntr_y, crop_cntr_x)
            label_img = self.crop_volume(label_img, crop_cntr_z, crop_cntr_y, crop_cntr_x)

        if self.intensity_norm:
            input_img = self.normalize_intensity(input_img)
            input_img = np.expand_dims(input_img, axis=0)

        label_img_channelized_shape = tuple([5] + list(np.shape(label_img)))
        label_img_channelized = np.zeros(shape=label_img_channelized_shape).astype(np.float32)

        for i in range(5):
            tmp = np.zeros(shape=np.shape(label_img))
            tmp[np.where(label_img == i)] = 1
            label_img_channelized[i, :, :, :] = tmp

        if self.swap_channel:
            input_img = self.swap_channel_order(input_img)
            label_img_channelized = self.swap_channel_order(label_img_channelized)

        input_img_list.append(input_img)
        label_img_list.append(label_img_channelized)

        return np.array(input_img_list), np.array(label_img_list)

    def next_minibatch(self, num_img):

        input_img_list = []
        label_img_list = []

        for n in range(num_img):
            idx = self.random_shuffle_table[self.idx_minibatch]
            fn_input = self.data_list[idx][0]
            fn_label = self.data_list[idx][1]
            hdf5_idx = self.data_list[idx][2]
            h5_reader = self.hdf5_reader_list[hdf5_idx]

            input_img = np.array(h5_reader.h5file[fn_input]).astype(np.float32)
            label_img = np.array(h5_reader.h5file[fn_label]).astype(np.float32)

            if self.augmentation_active:
                input_img, label_img = self.augmentation_on_the_fly(input_img, label_img)
            else:
                crop_cntr_z, crop_cntr_y, crop_cntr_x = (
                    int(np.shape(input_img)[0] / 2), int(np.shape(input_img)[1] / 2), int(np.shape(input_img)[2] / 2))
                input_img = self.crop_volume(input_img, crop_cntr_z, crop_cntr_y, crop_cntr_x)
                label_img = self.crop_volume(label_img, crop_cntr_z, crop_cntr_y, crop_cntr_x)

            if self.intensity_norm:
                input_img = self.normalize_intensity(input_img)
                input_img = np.expand_dims(input_img, axis=0)

            label_img_channelized_shape = tuple([5] + list(np.shape(label_img)))
            label_img_channelized = np.zeros(shape=label_img_channelized_shape).astype(np.float32)

            for i in range(5):
                tmp = np.zeros(shape=np.shape(label_img))
                tmp[np.where(label_img == i)] = 1
                label_img_channelized[i, :, :, :] = tmp

            if self.swap_channel:
                input_img = self.swap_channel_order(input_img)
                label_img_channelized = self.swap_channel_order(label_img_channelized)

            input_img_list.append(input_img)
            label_img_list.append(label_img_channelized)

            self.idx_minibatch += 1
            if self.idx_minibatch >= self.list_size:
                self.idx_minibatch = 0

        return np.array(input_img_list), np.array(label_img_list)

    def augmentation_on_the_fly(self, input_vol, label_vol):
        # random crop & scale

        if self.random_scale_range > 0:
            min_rescale_factor = max((self.target_dims[0] + 2) / np.shape(input_vol)[1], 1.0 - self.random_scale_range)
            rescale_factor = random.uniform(min_rescale_factor, 1.0 + self.random_scale_range)
            # rescale_factor = 0.8
            # rescaled_input_vol = []
            # for z in range(np.shape(input_vol)[0]):
            #     rescaled_input_vol.append(zoom(input_vol[z, :, :], rescale_factor, order=1, cval=-2048))
            rescaled_input_vol = zoom(input_vol, rescale_factor, order=3, cval=-1024)
            rescaled_label_vol = zoom(label_vol, rescale_factor, order=0, cval=0)

            if np.random.rand(1)[0] > 0.5:  # do not apply deformations always, just sometimes
                rescaled_input_vol, rescaled_label_vol = produceRandomlyDeformedImage(rescaled_input_vol,
                                                                                      rescaled_label_vol,
                                                                                      numcontrolpoints=2, stdDef=15)
        else:
            # rescale factor is equal to 1
            rescaled_input_vol = input_vol
            rescaled_label_vol = label_vol

        rescaled_shape = np.shape(rescaled_input_vol)

        random_crop_start_z = random.randint(0, rescaled_shape[0] - self.target_dims[0])
        random_crop_start_y = random.randint(0, rescaled_shape[1] - self.target_dims[1])
        random_crop_start_x = random.randint(0, rescaled_shape[2] - self.target_dims[2])

        cropped_input_vol = rescaled_input_vol[random_crop_start_z:random_crop_start_z + self.target_dims[0],
                            random_crop_start_y:random_crop_start_y + self.target_dims[1],
                            random_crop_start_x:random_crop_start_x + self.target_dims[2]
                            ]
        cropped_label_vol = rescaled_label_vol[random_crop_start_z:random_crop_start_z + self.target_dims[0],
                            random_crop_start_y:random_crop_start_y + self.target_dims[1],
                            random_crop_start_x:random_crop_start_x + self.target_dims[2]
                            ]

        # if self.random_rotflip_range > 0:
        #     idx_random_rotflip = random.randint(0, self.random_rotflip_range - 1)
        # else:
        #     idx_random_rotflip = 0
        # cropped_input_vol = rotations16_onlyz(cropped_input_vol, idx_random_rotflip)
        # cropped_label_vol = np.squeeze(rotations16_onlyz(np.expand_dims(cropped_label_vol, 0), idx_random_rotflip))

        # apply random offset
        cropped_input_vol = np.array(cropped_input_vol).astype(np.float32) + \
                            random.uniform(-self.random_intensity_offset_range, self.random_intensity_offset_range)

        return cropped_input_vol, cropped_label_vol

    def crop_volume(self, rescaled_input_vol, random_crop_cntr_z, random_crop_cntr_y, random_crop_cntr_x):

        vol_shape = np.shape(rescaled_input_vol)

        z1 = int(random_crop_cntr_z - self.target_dims[0] / 2)
        z2 = int(random_crop_cntr_z + self.target_dims[0] / 2)
        if z1 < 0:
            z2 -= z1
            z1 = 0
        if z2 >= vol_shape[0]:
            z1 -= (z2 - vol_shape[0] + 1)
            z2 = vol_shape[0] - 1

        y1 = int(random_crop_cntr_y - self.target_dims[1] / 2)
        y2 = int(random_crop_cntr_y + self.target_dims[1] / 2)
        if y1 < 0:
            y2 -= y1
            y1 = 0
        if y2 >= vol_shape[1]:
            y1 -= (y2 - vol_shape[1] + 1)
            y2 = vol_shape[1] - 1

        x1 = int(random_crop_cntr_x - self.target_dims[2] / 2)
        x2 = int(random_crop_cntr_x + self.target_dims[2] / 2)
        if x1 < 0:
            x2 -= x1
            x1 = 0
        if x2 >= vol_shape[2]:
            x1 -= (x2 - vol_shape[2] + 1)
            x2 = vol_shape[2] - 1

        cropped_vol = rescaled_input_vol[int(z1):int(z2), int(y1):int(y2), int(x1):int(x2)]
        return cropped_vol

    def swap_channel_order(self, vol):
        # return np.rollaxis(vol, 3, 0)
        return np.transpose(vol, [1, 2, 3, 0])


def produceRandomlyDeformedImage(image, label, numcontrolpoints, stdDef):
    sitkImage = sitk.GetImageFromArray(image, isVector=False)
    sitklabel = sitk.GetImageFromArray(label, isVector=False)

    transfromDomainMeshSize = [numcontrolpoints] * sitkImage.GetDimension()

    tx = sitk.BSplineTransformInitializer(sitkImage, transfromDomainMeshSize)

    params = tx.GetParameters()

    paramsNp = np.asarray(params, dtype=float)
    paramsNp = paramsNp + np.random.randn(paramsNp.shape[0]) * stdDef

    paramsNp[0:int(len(params) / 3)] = 0  # remove z deformations! The resolution in z is too bad

    params = tuple(paramsNp)
    tx.SetParameters(params)

    resampler_img = sitk.ResampleImageFilter()
    resampler_img.SetReferenceImage(sitkImage)
    resampler_img.SetInterpolator(sitk.sitkLinear)
    resampler_img.SetDefaultPixelValue(-1024)
    resampler_img.SetTransform(tx)

    resampler_label = sitk.ResampleImageFilter()
    resampler_label.SetReferenceImage(sitkImage)
    resampler_label.SetInterpolator(sitk.sitkNearestNeighbor)
    resampler_label.SetTransform(tx)
    resampler_label.SetDefaultPixelValue(0)

    outimgsitk = resampler_img.Execute(sitkImage)
    outlabsitk = resampler_label.Execute(sitklabel)

    outimg = sitk.GetArrayFromImage(outimgsitk)
    outimg = outimg.astype(dtype=np.float32)

    outlbl = sitk.GetArrayFromImage(outlabsitk)
    # outlbl = (outlbl > 0.5).astype(dtype=np.float32)
    outlbl = outlbl.astype(dtype=np.float32)

    return outimg, outlbl


def produceRandomlyTranslatedImage(image, label):
    sitkImage = sitk.GetImageFromArray(image, isVector=False)
    sitklabel = sitk.GetImageFromArray(label, isVector=False)

    itemindex = np.where(label > 0)
    randTrans = (0, np.random.randint(int(-np.min(itemindex[1]) / 2), (image.shape[1] - np.max(itemindex[1])) / 2),
                 np.random.randint(int(-np.min(itemindex[0]) / 2), (image.shape[0] - np.max(itemindex[0])) / 2))
    translation = sitk.TranslationTransform(3, randTrans)

    resampler = sitk.ResampleImageFilter()
    resampler.SetReferenceImage(sitkImage)
    resampler.SetInterpolator(sitk.sitkLinear)
    resampler.SetDefaultPixelValue(0)
    resampler.SetTransform(translation)

    outimgsitk = resampler.Execute(sitkImage)
    outlabsitk = resampler.Execute(sitklabel)

    outimg = sitk.GetArrayFromImage(outimgsitk)
    outimg = outimg.astype(dtype=float)

    outlbl = sitk.GetArrayFromImage(outlabsitk) > 0
    outlbl = outlbl.astype(dtype=float)

    return outimg, outlbl
