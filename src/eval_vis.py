import SimpleITK as sitk
import glob
import os
import numpy as np
from skimage import data, color, io, img_as_float
from src.run_eval import CCL

'''
Esophagus(1): Red
Heart(2): Magenta
Trachea(3): Blue -> Yellow
Aorta(4): Yellow -> Blue
'''


def colorize_vol(CT_raw, seg_raw, norm_wind_lower, norm_wind_upper, fn_out_fullpath=None):
    alpha = 0.8
    CT_raw_r = (CT_raw - norm_wind_lower) / (norm_wind_upper - norm_wind_lower)
    CT_raw_r[np.where(CT_raw_r < 0)] = 0
    CT_raw_r[np.where(CT_raw_r > 1)] = 1
    CT_raw_r = np.array(CT_raw_r)

    CT_raw_shape = np.shape(CT_raw)

    # for z in range(CT_raw_shape[0]):

    empty_mask = np.zeros(shape=np.shape(CT_raw_r))

    CT_raw_rgb = np.stack((CT_raw_r, CT_raw_r, CT_raw_r), axis=3)
    CT_raw_rgb_tmp = np.array(CT_raw_rgb)
    CT_raw_rgb_tmp[np.where(CT_raw_rgb_tmp < 0.8)] = 0.8
    total_mask_rgb = np.zeros(np.shape(CT_raw_rgb))
    for idx in range(1, 5):
        tmp_mask = np.zeros(shape=np.shape(CT_raw_r))
        tmp_mask[np.where(seg_raw == idx)] = 1
        if idx == 1:  # esophagus
            tmp_mask_rgb = np.stack((tmp_mask, empty_mask, empty_mask), axis=3)
        elif idx == 2:  # heart
            tmp_mask_rgb = np.stack((tmp_mask, empty_mask, tmp_mask), axis=3)
        elif idx == 3:  # trachea
            tmp_mask_rgb = np.stack((tmp_mask, tmp_mask, empty_mask), axis=3)
            # tmp_mask_rgb = np.stack((tmp_mask * 0.25, tmp_mask * 0.25, tmp_mask), axis=3)
            CT_raw_rgb[np.where(tmp_mask_rgb > 0)] = CT_raw_rgb_tmp[np.where(tmp_mask_rgb > 0)]
        elif idx == 4:  # aorta
            tmp_mask_rgb = np.stack((empty_mask, empty_mask, tmp_mask), axis=3)
            # tmp_mask_rgb = np.stack((tmp_mask * 0.7, tmp_mask * 0.7, empty_mask), axis=3)
            # CT_raw_rgb[np.where(tmp_mask_rgb > 0)] = CT_raw_rgb_tmp[np.where(tmp_mask_rgb > 0)]
        total_mask_rgb[np.where(tmp_mask_rgb > 0)] = tmp_mask_rgb[np.where(tmp_mask_rgb > 0)]

    # total_mask_rgb.tofile('\\\\data5\\Workspace\\haison\\Challenge_SegTHOR\\eval\\ckpt-66-valDice_0_0.985-valDice_1_0.581-valDice_2_0.872-valDice_3_0.680-valDice_4_0.728.h5\\Patient_01\\total_mask.raw')

    # CT_raw_rgb.tofile('\\\\data5\\Workspace\\haison\\Challenge_SegTHOR\\eval\\ckpt-66-valDice_0_0.985-valDice_1_0.581-valDice_2_0.872-valDice_3_0.680-valDice_4_0.728.h5\\Patient_01\\CT_raw_rgb.raw')
    img_masked = np.zeros(shape=np.shape(CT_raw_rgb))
    print('CT_raw_rgb shape: ' + str(np.shape(CT_raw_rgb)))

    for z in range(CT_raw_shape[0]):
        CT_raw_hsv = color.rgb2hsv(np.squeeze(CT_raw_rgb[z, :, :, :]))
        color_mask_hsv = color.rgb2hsv(np.squeeze(total_mask_rgb[z, :, :, :]))

        CT_raw_hsv[..., 0] = color_mask_hsv[..., 0]
        CT_raw_hsv[..., 1] = color_mask_hsv[..., 1] * alpha

        img_masked_tmp = color.hsv2rgb(CT_raw_hsv)
        img_masked[z, :, :, :] = img_masked_tmp

    if fn_out_fullpath is not None:
        np.array(img_masked * 255).astype(np.uint8).tofile(fn_out_fullpath)

    return np.array(img_masked * 255).astype(np.uint8)


def GetDice(GT_raw, pred_raw):
    GT_shape = np.shape(GT_raw)
    dice_list = []
    for idx in range(1, 5):
        GT_raw_label = np.zeros(GT_shape)
        pred_raw_label = np.zeros(GT_shape)
        GT_raw_label[np.where(GT_raw == idx)] = 1
        pred_raw_label[np.where(pred_raw == idx)] = 1

        intersect = GT_raw_label * pred_raw_label
        dice = 2 * np.sum(intersect) / (np.sum(GT_raw_label) + np.sum(pred_raw_label) + 1e-7)
        print('label %d DICE: %f' % (idx, dice))
        dice_list.append(dice)

    return dice_list


def main():
    for pid in range(8, 9):
        print('pid: %d' % pid)
        DIR_pred = '\\\\data5\\Workspace\\haison\\Challenge_SegTHOR\\eval\\ckpt-66-valDice_0_0.985-valDice_1_0.581-valDice_2_0.872-valDice_3_0.680-valDice_4_0.728.h5'

        fn_CT_mha = '\\\\data5\\ImageData\\MachineLearning\\Challenge_SegTHOR\\train_mha_resampled\\Patient_%02d.mha' % pid
        fn_GT_mha = '\\\\data5\\ImageData\\MachineLearning\\Challenge_SegTHOR\\train_mha_resampled\\Patient_%02d_GT.mha' % pid
        fn_pred = glob.glob(os.path.join(DIR_pred, 'Patient_%02d\\prediction_argmax_*.raw' % pid))[0]

        CT_img = sitk.ReadImage(fn_CT_mha)
        GT_img = sitk.ReadImage(fn_GT_mha)
        CT_raw = sitk.GetArrayFromImage(CT_img).astype(np.float32)
        GT_raw = sitk.GetArrayFromImage(GT_img).astype(np.int8)
        img_shape = np.shape(CT_raw)
        pred_raw = np.fromfile(fn_pred, dtype=np.int8, count=np.product(img_shape)).reshape(img_shape)

        # GT_on_CT = colorize_vol(CT_raw=CT_raw, seg_raw=GT_raw, norm_wind_lower=-1500-800, norm_wind_upper=1500-800)
        GT_on_CT = colorize_vol(CT_raw=CT_raw, seg_raw=GT_raw, norm_wind_lower=-1000, norm_wind_upper=1000)
        fn_GT_on_CT = os.path.join(DIR_pred, 'Patient_%02d' % pid,
                                   'GT_on_CT_%dx%dx%d.raw' % tuple(reversed(np.shape(GT_on_CT)[:3])))
        GT_on_CT.tofile(fn_GT_on_CT)

        pred_raw = CCL(pred_raw)
        fn_pred_ccl = os.path.join(DIR_pred, 'Patient_%02d\\prediction_argmax_ccl_%dx%dx%d.raw' % (
            pid, img_shape[2], img_shape[1], img_shape[0]))
        pred_raw.astype(np.int8).tofile(fn_pred_ccl)
        pred_on_CT = colorize_vol(CT_raw=CT_raw, seg_raw=pred_raw, norm_wind_lower=-1000, norm_wind_upper=1000)
        fn_pred_on_CT = os.path.join(DIR_pred, 'Patient_%02d' % pid,
                                     'pred_on_CT_cutoff_%dx%dx%d.raw' % tuple(reversed(np.shape(GT_on_CT)[:3])))
        pred_on_CT.tofile(fn_pred_on_CT)

        GetDice(GT_raw=GT_raw, pred_raw=pred_raw)


def main_for_only_test():
    for pid in range(41, 61):
        if pid != 60:
            continue
        print('pid: %d' % pid)
        # DIR_pred = '\\\\data5\\Workspace\\haison\\Challenge_SegTHOR\\eval\\DenseNet_seed_1101\\ckpt-171-valDice_0_0.992-valDice_1_0.687-valDice_2_0.916-valDice_3_0.727-valDice_4_0.779.h5'
        DIR_pred = '\\\\data5\\Workspace\\haison\\Challenge_SegTHOR\\eval\\DenseNet_seed_1101\\ckpt-329-valDice_0_0.993-valDice_1_0.714-valDice_2_0.925-valDice_3_0.742-valDice_4_0.784.h5'

        fn_CT_mha = '\\\\data4\\ImageData\\SegTHOR_challenge\\test\\Patient_%02d.nii.gz' % pid
        fn_pred = glob.glob(os.path.join(DIR_pred, 'Patient_%02d.nii\\prediction_argmax_ccl_*.raw' % pid))[0]

        CT_img = sitk.ReadImage(fn_CT_mha)
        CT_raw = sitk.GetArrayFromImage(CT_img).astype(np.float32)
        img_shape = np.shape(CT_raw)
        pred_raw = np.fromfile(fn_pred, dtype=np.int8, count=np.product(img_shape)).reshape(img_shape)
        # pred_raw = cutoff_pred(pred_raw)
        cutoff_list = [[38, 158], [38, 145], [27, 147], [73, 199], [27, 142],
                       [34, 170], [48, 166], [12, 139], [9, 104], [33, 155],
                       [17, 134], [38, 152], [19, 135], [10, 131], [28, 147],
                       [67, 182], [44, 160], [18, 134], [32, 156], [12, 166]]
        cutoff_list_idx = pid - 41
        pred_raw[0:cutoff_list[cutoff_list_idx][0], :, :] = 0
        pred_raw[cutoff_list[cutoff_list_idx][1]:, :, :] = 0

        pred_img = sitk.GetImageFromArray(pred_raw.astype(np.uint8))
        pred_img.SetOrigin(CT_img.GetOrigin())
        pred_img.SetDirection(CT_img.GetDirection())
        pred_img.SetSpacing(CT_img.GetSpacing())
        fn_pred_img = os.path.join('\\\\data5\\Workspace\\haison\\Challenge_SegTHOR\\eval\\DenseNet_seed_1101',
                                   'Patient_%02d.nii' % pid)
        sitk.WriteImage(pred_img, fn_pred_img, True)

        pred_on_CT = colorize_vol(CT_raw=CT_raw, seg_raw=pred_raw, norm_wind_lower=-1000, norm_wind_upper=1000)
        fn_pred_on_CT = os.path.join(DIR_pred, 'Patient_%02d.nii' % pid,
                                     'pred_on_CT_cutoff_manual_%dx%dx%d.raw' % tuple(
                                         reversed(np.shape(pred_on_CT)[:3])))
        pred_on_CT.tofile(fn_pred_on_CT)


def cutoff_pred(pred_raw):
    gt_shape = np.shape(pred_raw)
    start_end_slice = []
    for label in range(1, 5):

        binary_label = np.zeros(gt_shape).astype(np.uint8)
        binary_label[np.where(pred_raw == label)] = 1

        start_slice = 0
        for zz in range(0, gt_shape[0]):
            tmp_slice = binary_label[zz, :, :]
            tmp_sum = np.sum(tmp_slice)
            if tmp_sum > 0:
                start_slice = zz
                break

        end_slice = gt_shape[0] - 1
        for zz in reversed(range(0, gt_shape[0])):
            tmp_slice = binary_label[zz, :, :]
            tmp_sum = np.sum(tmp_slice)
            if tmp_sum > 0:
                end_slice = zz
                break
        start_end_slice += [start_slice, end_slice]

    heart_start = start_end_slice[2]
    aorta_end = start_end_slice[7]

    cutoff_1_start = 0
    cutoff_1_end = heart_start - 30
    if cutoff_1_start < cutoff_1_end:
        pred_raw[cutoff_1_start:cutoff_1_end, :, :] = 0

    cutoff_2_start = aorta_end + 30
    cutoff_2_end = gt_shape[0]
    if cutoff_2_start < cutoff_2_end:
        pred_raw[cutoff_2_start:cutoff_2_end, :, :] = 0

    return pred_raw


if __name__ == '__main__':
    # main()

    main_for_only_test()
