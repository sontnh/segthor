import numpy as np
import math


def rotations16_onlyz_for_bbox(pos_zyx, img_dim_zyx, idx_rot):
    zdim, ydim, xdim = img_dim_zyx
    zpos, ypos, xpos = pos_zyx

    cntr_x = (xdim - 1) / 2
    cntr_y = (ydim - 1) / 2
    cntr_z = (zdim - 1) / 2

    zpos_n = zpos - cntr_z  # zpos normalized
    ypos_n = ypos - cntr_y
    xpos_n = xpos - cntr_x

    zpos_r = 0  # zpos rotated
    ypos_r = 0
    xpos_r = 0

    zpos_d = 0  # zpos rotated and denormalized
    ypos_d = 0
    xpos_d = 0

    quo = idx_rot // 8
    rem = idx_rot % 8
    rot_angle_list = np.array([0, -90, -180, -270])
    rot_angle_list = np.deg2rad(rot_angle_list)
    if quo == 1:
        xpos_n = -xpos_n

    idx_rot_angle = rem % 4
    upside_down = rem // 4
    if upside_down == 1:
        zpos_n = -zpos_n
        xpos_n = -xpos_n

    zpos_r = zpos_n

    rot_angle_rad = rot_angle_list[idx_rot_angle]
    rot_mat = np.array([[math.cos(rot_angle_rad), math.sin(rot_angle_rad)],
                        [-math.sin(rot_angle_rad), math.cos(rot_angle_rad)]])
    xypos_r = np.matmul(rot_mat, [[xpos_n], [ypos_n]])

    zpos_d = zpos_r + cntr_z
    ypos_d = xypos_r[1] + cntr_y
    xpos_d = xypos_r[0] + cntr_x

    return np.array((zpos_d, ypos_d, xpos_d)).round().astype(np.int32)


def rotations16_onlyz(polycube, idx_rot):
    quo = idx_rot // 8
    rem = idx_rot % 8
    if quo == 0:
        return rotations24(polycube, rem)
    else:
        return rotations24(polycube[:, :, ::-1], rem)
        # return rotations24(polycube, rem)


def rotations48(polycube, idx_rot):
    quo = idx_rot // 24
    rem = idx_rot % 24
    if quo == 0:
        return rotations24(polycube, rem)
    else:
        return rotations24(polycube[:, :, ::-1], rem)
        # return rotations24(polycube, rem)


def rotations24(polycube, idx_rot):
    quo = idx_rot // 4
    rem = idx_rot % 4
    # imagine shape is pointing in axis 0 (up)

    if quo == 0:
        # 4 rotations about axis 0
        return rotations4(polycube, 0, rem)

    elif quo == 1:
        # rotate 180 about axis 1, now shape is pointing down in axis 0
        # 4 rotations about axis 0
        return rotations4(rot90(polycube, 2, axis=1), 0, rem)

    elif quo == 2:
        # rotate 90 or 270 about axis 1, now shape is pointing in axis 2
        # 8 rotations about axis 2
        return rotations4(rot90(polycube, axis=1), 2, rem)

    elif quo == 3:
        return rotations4(rot90(polycube, -1, axis=1), 2, rem)

    elif quo == 4:
        # rotate about axis 2, now shape is pointing in axis 1
        # 8 rotations about axis 1
        return rotations4(rot90(polycube, axis=2), 1, rem)
    elif quo == 5:
        return rotations4(rot90(polycube, -1, axis=2), 1, rem)


def rotations4(polycube, axis, idx_rot):
    """List the four rotations of the given cube about the given axis."""
    # for i in range(4):
    # print('in rot 4 idx_rot[%d]\n' % idx_rot)
    return rot90(polycube, idx_rot, axis)
    # mm =
    # # print(mm)
    # return mm


def rot90(m, k=1, axis=2):
    """Rotate an array by 90 degrees in the counter-clockwise direction around the given axis"""
    m = np.swapaxes(m, 2, axis)
    m = np.rot90(m, k)
    m = np.swapaxes(m, 2, axis)
    # print('==========================================')
    # print(m)
    # print('==========================================')
    return m
