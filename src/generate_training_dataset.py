import numpy as np
import SimpleITK as sitk
import os
import glob
from pathlib import Path
from src.util_HDF5 import HDF5Writer
import csv
import itertools


def main():
    # Gen_2Ddataset()
    Gen_3Ddataset()


def Gen_3Ddataset():
    DIR_SAVE = '\\\\data5\\ImageData\\MachineLearning\\Challenge_SegTHOR\\train_mha_resampled\\patch3D_wo_zlim'
    DIR_SAVE_hdf5 = '\\\\data5\\ImageData\\MachineLearning\\Challenge_SegTHOR\\train_mha_resampled\\patch3D_wo_zlim_h5'
    Path(DIR_SAVE).mkdir(parents=True, exist_ok=True)
    Path(DIR_SAVE_hdf5).mkdir(parents=True, exist_ok=True)
    patch_shape = [96, 192, 192]
    stride_shape = [32, 64, 64]
    # patch_shape = [80, 160, 160]
    # stride_shape = [16, 32, 32]

    for pat_idx in range(1, 41, 1):
        pat_id = 'Patient_%02d' % pat_idx
        fn_mha = '\\\\data5\\ImageData\\MachineLearning\\Challenge_SegTHOR\\train_mha_resampled\\%s.mha' % pat_id
        fn_gt = '\\\\data5\\ImageData\\MachineLearning\\Challenge_SegTHOR\\train_mha_resampled\\%s_GT.mha' % pat_id

        img_mha = sitk.ReadImage(fn_mha)
        img_gt = sitk.ReadImage(fn_gt)
        img_ct_raw = sitk.GetArrayFromImage(img_mha)
        img_gt_raw = sitk.GetArrayFromImage(img_gt)
        raw_gt = img_gt_raw
        raw_mha = img_ct_raw
        raw_mha_shape = np.shape(raw_mha)
        start_idx = 0
        end_idx = np.shape(raw_gt)[0]
        for i in range(np.shape(raw_gt)[0]):
            tmp = raw_gt[i, :, :]
            tmp_sum = np.sum(tmp)
            if tmp_sum > 0:
                start_idx = i
                break
        start_idx = max(start_idx, 0)

        for i in reversed(range(np.shape(raw_gt)[0])):
            tmp = raw_gt[i, :, :]
            tmp_sum = np.sum(tmp)
            if tmp_sum > 0:
                end_idx = i
                break
        end_idx = min(end_idx, np.shape(raw_gt)[0])

        # z_list = list(np.arange(start_idx, end_idx - patch_shape[0], stride_shape[0])) + [end_idx - patch_shape[0]]
        z_list = list(np.arange(0, raw_mha_shape[0] - patch_shape[0], stride_shape[0])) + [
            raw_mha_shape[0] - patch_shape[0]]
        y_list = list(np.arange(0, raw_mha_shape[1] - patch_shape[1], stride_shape[1])) + [
            raw_mha_shape[1] - patch_shape[1]]
        x_list = list(np.arange(0, raw_mha_shape[2] - patch_shape[2], stride_shape[2])) + [
            raw_mha_shape[2] - patch_shape[2]]

        # idx_patch = 0

        for idx_patch, (z, y, x) in enumerate(itertools.product(z_list, y_list, x_list)):
            if idx_patch % 30 == 0:
                print('Patient id[%d] \t idx_patch: %d / %d' % (
                    pat_idx, idx_patch, len(z_list) * len(y_list) * len(x_list)))

            patch_ct = raw_mha[z:z + patch_shape[0], y:y + patch_shape[1], x:x + patch_shape[2]]
            patch_gt = raw_gt[z:z + patch_shape[0], y:y + patch_shape[1], x:x + patch_shape[2]]
            fn_patch_mha = os.path.join(DIR_SAVE,
                                        '%s_%04d_CT_' % (pat_id, idx_patch) + '%dx%dx%d.raw' % tuple(
                                            reversed(np.shape(patch_ct))))
            fn_patch_gt = os.path.join(DIR_SAVE,
                                       '%s_%04d_GT_' % (pat_id, idx_patch) + '%dx%dx%d.raw' % tuple(
                                           reversed(np.shape(patch_gt))))
            np.array(patch_ct).astype(np.int16).tofile(fn_patch_mha)
            np.array(patch_gt).astype(np.int8).tofile(fn_patch_gt)

            idx_patch += 1

    ''' hdf5 process '''
    fn_ct_list = sorted(list(Path(DIR_SAVE).rglob("*_CT_*.[rR][aA][wW]")))
    fn_gt_list = sorted(list(Path(DIR_SAVE).rglob("*_GT_*.[rR][aA][wW]")))
    fn_hdf5 = os.path.join(DIR_SAVE_hdf5, 'training_data_3D.h5')
    fn_hdf5_csv = os.path.join(DIR_SAVE_hdf5, 'training_data_3D.h5csv')
    hdf5_writer = HDF5Writer(fn_hdf5)
    path_list_hdf5 = []
    for idx in range(len(fn_ct_list)):
        if idx % 100 == 0:
            print('idx : %d / %d' % (idx, len(fn_ct_list)))
        fn_ct = fn_ct_list[idx]
        fn_stem = Path(fn_ct).stem
        dtype = np.int16
        dim_str = fn_stem.split('_')[-1]
        dim_x = int(dim_str.split('x')[0])
        dim_y = int(dim_str.split('x')[1])
        dim_z = int(dim_str.split('x')[2])
        img_shape = (dim_z, dim_y, dim_x)
        data_ct = np.fromfile(str(fn_ct), dtype=dtype, count=np.product(img_shape)).reshape(img_shape)

        fn_gt = fn_gt_list[idx]
        fn_stem = Path(fn_gt).stem
        dtype = np.int8
        dim_str = fn_stem.split('_')[-1]
        dim_x = int(dim_str.split('x')[0])
        dim_y = int(dim_str.split('x')[1])
        dim_z = int(dim_str.split('x')[2])
        img_shape = (dim_z, dim_y, dim_x)
        data_gt = np.fromfile(str(fn_gt), dtype=dtype, count=np.product(img_shape)).reshape(img_shape)

        # threshold_value_on = dim_z * dim_y * dim_x / 4 / 4 / 4
        threshold_value_on = 0
        idcs_label_1 = np.where(data_gt[24:24 + 48, 48:48 + 96, 48:48 + 96] == 1)
        idcs_label_2 = np.where(data_gt[24:24 + 48, 48:48 + 96, 48:48 + 96] == 2)
        idcs_label_3 = np.where(data_gt[24:24 + 48, 48:48 + 96, 48:48 + 96] == 3)
        idcs_label_4 = np.where(data_gt[24:24 + 48, 48:48 + 96, 48:48 + 96] == 4)
        is_label_1_on = False
        is_label_2_on = False
        is_label_3_on = False
        is_label_4_on = False
        if len(idcs_label_1[0]) > threshold_value_on:
            is_label_1_on = True
            path_ct = 'label_1' + '/' + Path(fn_ct).name
            path_gt = 'label_1' + '/' + Path(fn_gt).name
            hdf5_writer.write_dataset_to_hdf5(path=path_ct, data=data_ct, compression=False)
            hdf5_writer.write_dataset_to_hdf5(path=path_gt, data=data_gt, compression=False)
            path_list_hdf5.append(path_ct)
            path_list_hdf5.append(path_gt)
            print('is_label_1_on: %s' % path_ct)

        if len(idcs_label_2[0]) > threshold_value_on:
            is_label_2_on = True
            path_ct = 'label_2' + '/' + Path(fn_ct).name
            path_gt = 'label_2' + '/' + Path(fn_gt).name
            hdf5_writer.write_dataset_to_hdf5(path=path_ct, data=data_ct, compression=False)
            hdf5_writer.write_dataset_to_hdf5(path=path_gt, data=data_gt, compression=False)
            path_list_hdf5.append(path_ct)
            path_list_hdf5.append(path_gt)

        if len(idcs_label_3[0]) > threshold_value_on:
            is_label_3_on = True
            path_ct = 'label_3' + '/' + Path(fn_ct).name
            path_gt = 'label_3' + '/' + Path(fn_gt).name
            hdf5_writer.write_dataset_to_hdf5(path=path_ct, data=data_ct, compression=False)
            hdf5_writer.write_dataset_to_hdf5(path=path_gt, data=data_gt, compression=False)
            path_list_hdf5.append(path_ct)
            path_list_hdf5.append(path_gt)
            print('is_label_3_on: %s' % path_ct)

        if len(idcs_label_4[0]) > threshold_value_on:
            is_label_4_on = True
            path_ct = 'label_4' + '/' + Path(fn_ct).name
            path_gt = 'label_4' + '/' + Path(fn_gt).name
            hdf5_writer.write_dataset_to_hdf5(path=path_ct, data=data_ct, compression=False)
            hdf5_writer.write_dataset_to_hdf5(path=path_gt, data=data_gt, compression=False)
            path_list_hdf5.append(path_ct)
            path_list_hdf5.append(path_gt)

        if (not is_label_1_on) and (not is_label_2_on) and (not is_label_3_on) and (not is_label_4_on):
            path_ct = 'label_0' + '/' + Path(fn_ct).name
            path_gt = 'label_0' + '/' + Path(fn_gt).name
            hdf5_writer.write_dataset_to_hdf5(path=path_ct, data=data_ct, compression=False)
            hdf5_writer.write_dataset_to_hdf5(path=path_gt, data=data_gt, compression=False)
            path_list_hdf5.append(path_ct)
            path_list_hdf5.append(path_gt)

    # for idx_fn, fn in enumerate(fn_list):
    #     if idx_fn % 100 == 0:
    #         print('hdf5 process idx: %d / %d' % (idx_fn + 1, len(fn_list)))
    #     fn_stem = Path(fn).stem
    #     if '_CT_' in str(fn):
    #         dtype = np.int16
    #         dim_str = fn_stem.split('_')[-1]
    #         dim_x = int(dim_str.split('x')[0])
    #         dim_y = int(dim_str.split('x')[1])
    #         dim_z = int(dim_str.split('x')[2])
    #         img_shape = (dim_z, dim_y, dim_x)
    #         kind = 'CT'
    #     else:
    #         dtype = np.int8
    #         dim_str = fn_stem.split('_')[-1]
    #         dim_x = int(dim_str.split('x')[0])
    #         dim_y = int(dim_str.split('x')[1])
    #         dim_z = int(dim_str.split('x')[2])
    #         img_shape = (dim_z, dim_y, dim_x)
    #         kind = 'GT'
    #
    #     data = np.fromfile(str(fn), dtype=dtype, count=np.product(img_shape)).reshape(img_shape)
    #     path = kind + '/' + Path(fn).name
    #     hdf5_writer.write_dataset_to_hdf5(path=path, data=data, compression=False)
    #     path_list_hdf5.append(path)

    with open(fn_hdf5_csv, 'w', newline='') as fid:
        writer = csv.writer(fid)
        for path in path_list_hdf5:
            writer.writerow([path])


def Gen_2Ddataset():
    DIR_SAVE = '\\\\data5\\ImageData\\MachineLearning\\Challenge_SegTHOR\\train_mha_resampled\\patch'
    DIR_SAVE_Taxial = os.path.join(DIR_SAVE, 'transaxial')
    DIR_SAVE_Coronal = os.path.join(DIR_SAVE, 'coronal')
    DIR_SAVE_Sagittal = os.path.join(DIR_SAVE, 'sagittal')
    Path(DIR_SAVE_Taxial).mkdir(parents=True, exist_ok=True)
    Path(DIR_SAVE_Coronal).mkdir(parents=True, exist_ok=True)
    Path(DIR_SAVE_Sagittal).mkdir(parents=True, exist_ok=True)
    # for pat_idx in range(1, 41, 1):
    #     pat_id = 'Patient_%02d' % pat_idx
    #     fn_mha = '\\\\data5\\ImageData\\MachineLearning\\Challenge_SegTHOR\\train_mha_resampled\\%s.mha' % pat_id
    #     fn_gt = '\\\\data5\\ImageData\\MachineLearning\\Challenge_SegTHOR\\train_mha_resampled\\%s_GT.mha' % pat_id
    #
    #     img_mha = sitk.ReadImage(fn_mha)
    #     img_gt = sitk.ReadImage(fn_gt)
    #
    #     raw_mha = np.array(sitk.GetArrayFromImage(img_mha)).astype(np.int16)
    #     raw_gt = np.array(sitk.GetArrayFromImage(img_gt)).astype(np.int8)
    #
    #     print('load Done (%s) ...' % fn_mha)
    #
    #     process_transaxial(raw_mha, raw_gt, pat_id, DIR_SAVE_Taxial)
    #     process_coronal(raw_mha, raw_gt, pat_id, DIR_SAVE_Coronal)
    #     process_sagittal(raw_mha, raw_gt, pat_id, DIR_SAVE_Sagittal)
    ''' hdf5 process '''
    fn_list = sorted(list(Path(DIR_SAVE).rglob("*.[rR][aA][wW]")))
    fn_hdf5 = os.path.join(DIR_SAVE, 'training_data.h5')
    fn_hdf5_csv = os.path.join(DIR_SAVE, 'training_data.h5csv')
    hdf5_writer = HDF5Writer(fn_hdf5)
    path_list_hdf5 = []
    for idx_fn, fn in enumerate(fn_list):
        if idx_fn % 100 == 0:
            print('hdf5 process idx: %d / %d' % (idx_fn + 1, len(fn_list)))
        fn_stem = Path(fn).stem
        if '_CT_' in str(fn):
            dtype = np.int16
            dim_str = fn_stem.split('_')[-1]
            dim_x = int(dim_str.split('x')[0])
            dim_y = int(dim_str.split('x')[1])
            dim_z = int(dim_str.split('x')[2])
            img_shape = (dim_z, dim_y, dim_x)
        else:
            dtype = np.int8
            dim_str = fn_stem.split('_')[-1]
            dim_x = int(dim_str.split('x')[0])
            dim_y = int(dim_str.split('x')[1])
            img_shape = (dim_y, dim_x)

        data = np.fromfile(str(fn), dtype=dtype, count=np.product(img_shape)).reshape(img_shape)
        kind = str(fn).split(os.sep)[-2]
        path = kind + '/' + Path(fn).name
        hdf5_writer.write_dataset_to_hdf5(path=path, data=data, compression=False)
        path_list_hdf5.append(path)
    with open(fn_hdf5_csv, 'w', newline='') as fid:
        writer = csv.writer(fid)
        for path in path_list_hdf5:
            writer.writerow([path])


def process_sagittal(raw_mha, raw_gt, pat_id, DIR_SAVE_Sagittal):
    ''' Sagittal '''
    raw_mha_sa = np.transpose(raw_mha, axes=[2, 0, 1])
    raw_gt_sa = np.transpose(raw_gt, axes=[2, 0, 1])
    start_idx = 0
    end_idx = np.shape(raw_gt_sa)[0]
    for i in range(np.shape(raw_gt_sa)[0]):
        tmp = raw_gt_sa[i, :, :]
        tmp_sum = np.sum(tmp)
        if tmp_sum > 0:
            start_idx = i
            break
    start_idx = max(start_idx, 1)

    for i in reversed(range(np.shape(raw_gt_sa)[0])):
        tmp = raw_gt_sa[i, :, :]
        tmp_sum = np.sum(tmp)
        if tmp_sum > 0:
            end_idx = i
            break
    end_idx = min(end_idx, np.shape(raw_gt_sa)[0] - 1)

    for i in range(start_idx, end_idx):
        mha_patch = raw_mha_sa[i - 1:i + 2, :, :]
        gt_patch = raw_gt_sa[i, :, :]
        fn_patch_mha = os.path.join(DIR_SAVE_Sagittal,
                                    '%s_S_%04d_CT_' % (pat_id, i) + '%dx%dx%d.raw' % tuple(
                                        reversed(np.shape(mha_patch))))
        fn_patch_gt = os.path.join(DIR_SAVE_Sagittal,
                                   '%s_S_%04d_GT_' % (pat_id, i) + '%dx%d.raw' % tuple(reversed(np.shape(gt_patch))))

        np.array(mha_patch).astype(np.int16).tofile(fn_patch_mha)
        np.array(gt_patch).astype(np.int8).tofile(fn_patch_gt)


def process_coronal(raw_mha, raw_gt, pat_id, DIR_SAVE_Coronal):
    ''' Coronal '''
    raw_mha_co = np.transpose(raw_mha, axes=[1, 0, 2])
    raw_gt_co = np.transpose(raw_gt, axes=[1, 0, 2])
    start_idx = 0
    end_idx = np.shape(raw_gt_co)[0]
    for i in range(np.shape(raw_gt_co)[0]):
        tmp = raw_gt_co[i, :, :]
        tmp_sum = np.sum(tmp)
        if tmp_sum > 0:
            start_idx = i
            break
    start_idx = max(start_idx, 1)

    for i in reversed(range(np.shape(raw_gt_co)[0])):
        tmp = raw_gt_co[i, :, :]
        tmp_sum = np.sum(tmp)
        if tmp_sum > 0:
            end_idx = i
            break
    end_idx = min(end_idx, np.shape(raw_gt_co)[0] - 1)

    for i in range(start_idx, end_idx):
        mha_patch = raw_mha_co[i - 1:i + 2, :, :]
        gt_patch = raw_gt_co[i, :, :]
        fn_patch_mha = os.path.join(DIR_SAVE_Coronal,
                                    '%s_C_%04d_CT_' % (pat_id, i) + '%dx%dx%d.raw' % tuple(
                                        reversed(np.shape(mha_patch))))
        fn_patch_gt = os.path.join(DIR_SAVE_Coronal,
                                   '%s_C_%04d_GT_' % (pat_id, i) + '%dx%d.raw' % tuple(reversed(np.shape(gt_patch))))

        np.array(mha_patch).astype(np.int16).tofile(fn_patch_mha)
        np.array(gt_patch).astype(np.int8).tofile(fn_patch_gt)


def process_transaxial(raw_mha, raw_gt, pat_id, DIR_SAVE_Taxial):
    ''' Transaxial '''
    start_idx = 0
    end_idx = np.shape(raw_gt)[0]
    for i in range(np.shape(raw_gt)[0]):
        tmp = raw_gt[i, :, :]
        tmp_sum = np.sum(tmp)
        if tmp_sum > 0:
            start_idx = i
            break
    start_idx = max(start_idx, 1)

    for i in reversed(range(np.shape(raw_gt)[0])):
        tmp = raw_gt[i, :, :]
        tmp_sum = np.sum(tmp)
        if tmp_sum > 0:
            end_idx = i
            break
    end_idx = min(end_idx, np.shape(raw_gt)[0] - 1)

    for i in range(start_idx, end_idx, 1):
        mha_patch = raw_mha[i - 1:i + 2, :, :]
        gt_patch = raw_gt[i, :, :]
        fn_patch_mha = os.path.join(DIR_SAVE_Taxial,
                                    '%s_T_%04d_CT_' % (pat_id, i) + '%dx%dx%d.raw' % tuple(
                                        reversed(np.shape(mha_patch))))
        fn_patch_gt = os.path.join(DIR_SAVE_Taxial,
                                   '%s_T_%04d_GT_' % (pat_id, i) + '%dx%d.raw' % tuple(reversed(np.shape(gt_patch))))

        np.array(mha_patch).astype(np.int16).tofile(fn_patch_mha)
        np.array(gt_patch).astype(np.int8).tofile(fn_patch_gt)


if __name__ == '__main__':
    main()
