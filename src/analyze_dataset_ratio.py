import glob
import os
from pathlib import Path
import numpy as np

DIR_SRC = '\\\\data5\\ImageData\\MachineLearning\\Challenge_SegTHOR\\train_mha_resampled\\patch\\transaxial'


def main():
    fn_list = sorted(glob.glob(os.path.join(DIR_SRC, '*GT*')))

    count = [0, 0, 0, 0, 0]

    for idx_fn, fn in enumerate(fn_list[:]):
        if idx_fn % 100 == 0:
            print('idx_fn: %d / %d' % (idx_fn, len(fn_list)))

        fn_stem = Path(fn).stem
        dim_str = str(fn_stem).split('_')[-1]
        dim_y = int(dim_str.split('x')[1])
        dim_x = int(dim_str.split('x')[0])
        data_shape = (dim_y, dim_x)
        data = np.fromfile(fn, dtype=np.uint8, count=np.product(data_shape)).reshape(data_shape)

        for i in range(5):
            idcs = np.where(data == i)
            count[i] += len(idcs[0])

    print('count: ' + str(count))
    val_sum = np.sum(count)
    count_ratio = [x / val_sum for x in count]
    print('count(ratio): ' + str(count_ratio))


if __name__ == '__main__':
    main()
