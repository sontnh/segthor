import os
import glob
import io
import yaml
import numpy as np
import random
from pathlib import Path

from src.util_args import args, FN_CONFIG
from src.mylogger import training_logger
from src.SimpleReaderHDF5 import SimpleReaderHDF5


def main():
    intensity_norm = True
    norm_wind_lower = -1000
    norm_wind_upper = 2000

    dir_training_data = args.dir_training_data
    dir_test_data = args.dir_test_data

    fn_pattern_test_input = args.fn_pattern_test_input
    fn_pattern_test_label = args.fn_pattern_test_label

    fn_pattern_valid_input_list = [args.fn_pattern_valid_input_0,
                                   args.fn_pattern_valid_input_1,
                                   args.fn_pattern_valid_input_2,
                                   args.fn_pattern_valid_input_3,
                                   args.fn_pattern_valid_input_4,
                                   ]
    fn_pattern_valid_label_list = [args.fn_pattern_valid_label_0,
                                   args.fn_pattern_valid_label_1,
                                   args.fn_pattern_valid_label_2,
                                   args.fn_pattern_valid_label_3,
                                   args.fn_pattern_valid_label_4,
                                   ]

    fn_pattern_train_input_list = [args.fn_pattern_train_input_0,
                                   args.fn_pattern_train_input_1,
                                   args.fn_pattern_train_input_2,
                                   args.fn_pattern_train_input_3,
                                   args.fn_pattern_train_input_4,
                                   ]
    fn_pattern_train_label_list = [args.fn_pattern_train_label_0,
                                   args.fn_pattern_train_label_1,
                                   args.fn_pattern_train_label_2,
                                   args.fn_pattern_train_label_3,
                                   args.fn_pattern_train_label_4,
                                   ]

    swap_channel = True

    # target_dims = (144, 480)  # for coronal or sagittal
    # target_dims = (480, 480)  # for transaxial
    target_dims = (64, 128, 128)  # for transaxial

    '''valid patch pre-load'''
    fn_h5_valid_list = glob.glob(os.path.join(dir_test_data, '*.h5'))
    reader_valid_list = []
    for idx in range(len(fn_pattern_valid_input_list)):
        fn_pattern_valid_input = fn_pattern_valid_input_list[idx]
        fn_pattern_valid_label = fn_pattern_valid_label_list[idx]

        reader_valid = SimpleReaderHDF5(fn_h5_valid_list, target_dims=target_dims,
                                        input_pattern_regex=fn_pattern_valid_input,
                                        label_pattern_regex=fn_pattern_valid_label,
                                        random_shuffle=True, intensity_norm=intensity_norm,
                                        norm_wind_lower=norm_wind_lower,
                                        norm_wind_upper=norm_wind_upper, swap_channel=swap_channel,
                                        augmentation_onthefly_active=False)
        print('valid_label_%d: %d' % (idx, reader_valid.get_list_size()))
        reader_valid_list.append(reader_valid)

    '''train patch pre-load'''
    fn_h5_train_list = glob.glob(os.path.join(dir_training_data, '*.h5'))
    reader_train_list = []
    for idx in range(len(fn_pattern_train_input_list)):
        fn_pattern_train_input = fn_pattern_train_input_list[idx]
        fn_pattern_train_label = fn_pattern_train_label_list[idx]
        reader_train = SimpleReaderHDF5(fn_h5_train_list, target_dims=target_dims,
                                        input_pattern_regex=fn_pattern_train_input,
                                        label_pattern_regex=fn_pattern_train_label,
                                        random_shuffle=True, intensity_norm=intensity_norm,
                                        norm_wind_lower=norm_wind_lower,
                                        norm_wind_upper=norm_wind_upper, swap_channel=swap_channel,
                                        augmentation_onthefly_active=True)
        print('train_label_%d: %d' % (idx, reader_train.get_list_size()))
        reader_train_list.append(reader_train)

    model_dir = args.model_out_dir
    Path(model_dir).mkdir(parents=True, exist_ok=True)
    if args.chkpnt == 'None':
        restore_fn_dnn = None
    else:
        restore_fn_dnn = os.path.join(model_dir, args.chkpnt)

    tensorboard_logdir = os.path.join(model_dir, 'tensorboard_log')
    Path(tensorboard_logdir).mkdir(parents=True, exist_ok=True)

    ''' Write args.parameters to model_out_dir '''
    fn_config_out = os.path.join(model_dir, '()' + Path(FN_CONFIG).name)
    with io.open(fn_config_out, 'w', encoding='utf8') as fid:
        args_dict = args.__dict__
        yaml.dump(args_dict, fid, default_flow_style=False, allow_unicode=True)

    np.random.seed(args.seed)

    epoch_size = args.epoch_size
    total_epochs = args.total_epochs
    minibatch_size = args.minibatch_size

    from src.train_SegthorNet import train_by_keras
    train_by_keras(reader_train_list=reader_train_list, reader_valid_list=reader_valid_list, epoch_size=epoch_size,
                   total_epochs=total_epochs, minibatch_size=minibatch_size, model_dir=model_dir,
                   tensorboard_logdir=tensorboard_logdir, input_dim=target_dims, restore_fn_dnn=restore_fn_dnn,
                   args=args)


if __name__ == '__main__':
    os.environ['CUDA_VISIBLE_DEVICES'] = '%d' % args.dev
    main()
