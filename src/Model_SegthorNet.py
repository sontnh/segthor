from keras.models import Model
from keras.layers import (Activation, Convolution2D, BatchNormalization, Concatenate, Conv2DTranspose, Softmax, Lambda)
from keras.layers import (Convolution3D)
import keras
import tensorflow as tf


def slicechannel_lamda(x, start, end):
    return x[:, :, :, start:end]


def _phase_shift_3D(I, r):
    bsize, z, y, x, c = I.get_shape().as_list()
    bsize = tf.shape(I)[0]  # Handling Dimension(None) type for undefined batch dim
    X = tf.reshape(I, (bsize, z, y, x, r, r, r))

    X = tf.transpose(X, (0, 1, 2, 3, 6, 5, 4))  # bsize, z, y, x, 1, 1, 1
    X = tf.split(X, z, axis=1)  # z, [bsize, 1, y, x, r, r, r]
    X = tf.concat([tf.squeeze(x, axis=1) for x in X], axis=3)  # bsize, y, x, z*r, r, r

    X = tf.split(X, y, axis=1)  # y, [bsize, x, z*r, r, r]
    X = tf.concat([tf.squeeze(x, axis=1) for x in X], axis=3)  # bsize, x, z*r, y*r, r

    X = tf.split(X, x, axis=1)  # x, [bsize, z*r, y*r, r]
    X = tf.concat([tf.squeeze(x, axis=1) for x in X], axis=3)  # bsize, z*r, y*r, x*r

    return tf.reshape(X, (bsize, z * r, y * r, x * r, 1))


def subpixel_2_3D(**kwargs):
    return Lambda(lambda x: _phase_shift_3D(x, 2), **kwargs)


def subpixel_4_3D(**kwargs):
    return Lambda(lambda x: _phase_shift_3D(x, 4), **kwargs)


def subpixel_8_3D(**kwargs):
    return Lambda(lambda x: _phase_shift_3D(x, 8), **kwargs)


def _phase_shift_3D_wChannel(I, r, out_c):
    bsize, z, y, x, c = I.get_shape().as_list()
    bsize = tf.shape(I)[0]  # Handling Dimension(None) type for undefined batch dim
    X = tf.reshape(I, (bsize, z, y, x, r, r, r, out_c))

    X = tf.transpose(X, (0, 1, 2, 3, 6, 5, 4, 7))  # bsize, z, y, x, 1, 1, 1
    X = tf.split(X, z, axis=1)  # z, [bsize, 1, y, x, r, r, r, out_c]
    X = tf.concat([tf.squeeze(x, axis=1) for x in X], axis=3)  # bsize, y, x, z*r, r, r

    X = tf.split(X, y, axis=1)  # y, [bsize, x, z*r, r, r, out_c]
    X = tf.concat([tf.squeeze(x, axis=1) for x in X], axis=3)  # bsize, x, z*r, y*r, r

    X = tf.split(X, x, axis=1)  # x, [bsize, z*r, y*r, r, out_c]
    X = tf.concat([tf.squeeze(x, axis=1) for x in X], axis=3)  # bsize, z*r, y*r, x*r

    return tf.reshape(X, (bsize, z * r, y * r, x * r, out_c))


def subpixel_2_3D_wChannel(out_c, **kwargs):
    return Lambda(lambda x: _phase_shift_3D_wChannel(x, 2, out_c), **kwargs)


def subpixel_4_3D_wChannel(out_c, **kwargs):
    return Lambda(lambda x: _phase_shift_3D_wChannel(x, 4, out_c), **kwargs)


class SegthorNet:
    def __init__(self, input, network_name):
        self.concat_axis = -1
        self.out = getattr(self, network_name)(input)
        self.model = Model(inputs=input, outputs=self.out)

    def dense_block(self, input_tensor, nb_layers, growth_k):
        output_intermediate = self.bottleneck_layer(input_tensor=input_tensor, growth_k=growth_k)
        output_tensor = Concatenate(axis=self.concat_axis)([input_tensor, output_intermediate])

        for idx in range(1, nb_layers):
            output_intermediate = self.bottleneck_layer(output_tensor, growth_k=growth_k)
            output_tensor = Concatenate(axis=self.concat_axis)([output_tensor, output_intermediate])

        return output_tensor

    def bottleneck_layer(self, input_tensor, growth_k):
        out_bn_1 = BatchNormalization(axis=self.concat_axis, center=True)(input_tensor)
        out_relu_1 = Activation('relu')(out_bn_1)

        out_conv_1 = Convolution3D(filters=growth_k * 4, kernel_size=(1, 1, 1), strides=(1, 1, 1), padding='same',
                                   use_bias=True, kernel_initializer='he_normal',
                                   kernel_regularizer=None)(out_relu_1)

        out_bn_2 = BatchNormalization(axis=self.concat_axis, center=True)(out_conv_1)
        out_relu_2 = Activation('relu')(out_bn_2)

        output_tensor = Convolution3D(filters=growth_k, kernel_size=(3, 3, 3), strides=(1, 1, 1), padding='same',
                                      use_bias=True, kernel_initializer='he_normal',
                                      kernel_regularizer=None)(out_relu_2)

        return output_tensor

    def transition_layer(self, input_tensor, theta=0.5):
        nb_channel = int(input_tensor.shape[-1])
        out_bn_1 = BatchNormalization(axis=self.concat_axis, center=True)(input_tensor)
        out_conv_1 = Convolution3D(filters=int(nb_channel * theta), kernel_size=(1, 1, 1), strides=(1, 1, 1),
                                   padding='same', use_bias=True, kernel_initializer='he_normal',
                                   kernel_regularizer=None)(out_bn_1)
        return out_conv_1

    def last_label_layer(self, input_tensor):
        bn_1 = BatchNormalization(axis=self.concat_axis, center=True)(input_tensor)
        relu_1 = Activation('relu')(bn_1)
        conv_1 = Convolution3D(filters=256, kernel_size=(3, 3, 3), strides=(1, 1, 1), padding='same',
                               use_bias=True, kernel_initializer='he_normal',
                               kernel_regularizer=None)(relu_1)

        bn_2 = BatchNormalization(axis=self.concat_axis, center=True)(conv_1)
        relu_2 = Activation('relu')(bn_2)
        conv_2 = Convolution3D(filters=512, kernel_size=(3, 3, 3), strides=(1, 1, 1), padding='same',
                               use_bias=True, kernel_initializer='he_normal',
                               kernel_regularizer=None)(relu_2)
        return subpixel_8_3D()(conv_2)

    def last_label_layer_for_DenseNetv2(self, input_tensor):
        bn_1 = BatchNormalization(axis=self.concat_axis, center=True)(input_tensor)
        relu_1 = Activation('relu')(bn_1)
        conv_1 = Convolution3D(filters=16, kernel_size=(3, 3, 3), strides=(1, 1, 1), padding='same',
                               use_bias=True, kernel_initializer='he_normal',
                               kernel_regularizer=None)(relu_1)

        bn_2 = BatchNormalization(axis=self.concat_axis, center=True)(conv_1)
        relu_2 = Activation('relu')(bn_2)
        conv_2 = Convolution3D(filters=8, kernel_size=(3, 3, 3), strides=(1, 1, 1), padding='same',
                               use_bias=True, kernel_initializer='he_normal',
                               kernel_regularizer=None)(relu_2)
        return subpixel_2_3D_wChannel(out_c=1)(conv_2)

    def last_label_layer_for_DenseNetv3(self, input_tensor):
        bn_1 = BatchNormalization(axis=self.concat_axis, center=True)(input_tensor)
        relu_1 = Activation('relu')(bn_1)
        conv_1 = Convolution3D(filters=192, kernel_size=(3, 3, 3), strides=(1, 1, 1), padding='same',
                               use_bias=True, kernel_initializer='he_normal',
                               kernel_regularizer=None)(relu_1)

        bn_2 = BatchNormalization(axis=self.concat_axis, center=True)(conv_1)
        relu_2 = Activation('relu')(bn_2)
        conv_2 = Convolution3D(filters=64, kernel_size=(3, 3, 3), strides=(1, 1, 1), padding='same',
                               use_bias=True, kernel_initializer='he_normal',
                               kernel_regularizer=None)(relu_2)
        return subpixel_4_3D_wChannel(out_c=1)(conv_2)

    def DenseNet_v1(self, input):
        growth_k = 8
        # growth_k = 4
        theta = 0.5
        conv_0 = Convolution3D(filters=growth_k * 2, kernel_size=(7, 7, 7), strides=(2, 2, 2), padding='same',
                               use_bias=True, kernel_initializer='he_normal',
                               kernel_regularizer=None)(input)  # 32 x 64 x 64
        # init_bn_0 = BatchNormalization(axis=self.concat_axis, center=False)(conv_0)
        # init_relu_0 = Activation('relu')(init_bn_0)
        #
        # conv_1 = Convolution3D(filters=growth_k * 2, kernel_size=(7, 7, 7), strides=(2, 2, 2), padding='same',
        #                        use_bias=True, kernel_initializer='he_normal',
        #                        kernel_regularizer=None)(init_relu_0)    # 16 x 32 x 32

        nb_layers = 12
        denseblk_1 = self.dense_block(conv_0, nb_layers=nb_layers, growth_k=growth_k)
        transit_1 = self.transition_layer(denseblk_1, theta=theta)
        bsize, zz, yy, xx, c = transit_1.get_shape().as_list()
        transit_1 = Convolution3D(filters=c, kernel_size=(3, 3, 3), strides=(2, 2, 2), padding='same',
                                  use_bias=True, kernel_initializer='he_normal',
                                  kernel_regularizer=None)(transit_1)  # 16 x 32 x 32

        nb_layers = 24
        denseblk_2 = self.dense_block(transit_1, nb_layers=nb_layers, growth_k=growth_k)
        transit_2 = self.transition_layer(denseblk_2, theta=theta)
        bsize, zz, yy, xx, c = transit_2.get_shape().as_list()
        transit_2 = Convolution3D(filters=c, kernel_size=(3, 3, 3), strides=(2, 2, 2), padding='same',
                                  use_bias=True, kernel_initializer='he_normal',
                                  kernel_regularizer=None)(transit_2)  # 8 x 16 x 16

        nb_layers = 18
        denseblk_3 = self.dense_block(transit_2, nb_layers=nb_layers, growth_k=growth_k)

        label_0 = self.last_label_layer(denseblk_3)
        label_1 = self.last_label_layer(denseblk_3)
        label_2 = self.last_label_layer(denseblk_3)
        label_3 = self.last_label_layer(denseblk_3)
        label_4 = self.last_label_layer(denseblk_3)
        label = Concatenate()([label_0, label_1, label_2, label_3, label_4])
        # label = Concatenate()([label_0, label_0, label_0, label_0, label_0])

        return label

    def DenseNet_v2(self, input):
        growth_k = 8
        # growth_k = 4
        theta = 0.5
        conv_0 = Convolution3D(filters=growth_k * 2, kernel_size=(7, 7, 7), strides=(2, 2, 2), padding='same',
                               use_bias=True, kernel_initializer='he_normal',
                               kernel_regularizer=None)(input)  # 32 x 64 x 64 (ch: 16)

        nb_layers = 12
        denseblk_1 = self.dense_block(conv_0, nb_layers=nb_layers, growth_k=growth_k)
        transit_1 = self.transition_layer(denseblk_1, theta=theta)
        bsize, zz, yy, xx, c = transit_1.get_shape().as_list()
        transit_1 = Convolution3D(filters=c, kernel_size=(3, 3, 3), strides=(2, 2, 2), padding='same',
                                  use_bias=True, kernel_initializer='he_normal',
                                  kernel_regularizer=None)(transit_1)  # 16 x 32 x 32 (ch: 56 = (16 + 96)/2)

        nb_layers = 24
        denseblk_2 = self.dense_block(transit_1, nb_layers=nb_layers, growth_k=growth_k)
        transit_2 = self.transition_layer(denseblk_2, theta=theta)
        bsize, zz, yy, xx, c = transit_2.get_shape().as_list()
        transit_2 = Convolution3D(filters=c, kernel_size=(3, 3, 3), strides=(2, 2, 2), padding='same',
                                  use_bias=True, kernel_initializer='he_normal',
                                  kernel_regularizer=None)(transit_2)  # 8 x 16 x 16 (ch: 124 = (56 + 24*8)

        nb_layers = 18
        denseblk_3 = self.dense_block(transit_2, nb_layers=nb_layers,
                                      growth_k=growth_k)  # 8 x 16 x 16 (ch: 268: 124 + 18*8)
        bn_1 = BatchNormalization(axis=self.concat_axis, center=True)(denseblk_3)
        relu_1 = Activation('relu')(bn_1)
        transit_3 = Convolution3D(filters=512, kernel_size=(3, 3, 3), strides=(1, 1, 1), padding='same',
                                  use_bias=True, kernel_initializer='he_normal',
                                  kernel_regularizer=None)(relu_1)  # 8 x 16 x 16 (ch: 124 = (56 + 24*8)
        transit_3_2up = subpixel_2_3D_wChannel(64)(transit_3)

        transit_1_transit_3_2up = Concatenate()([transit_1, transit_3_2up])  # 16 x 32 x 32 (ch: 120)
        nb_layers = 12
        denseblk_4 = self.dense_block(transit_1_transit_3_2up, nb_layers=nb_layers,
                                      growth_k=growth_k)  # 16 x 32 x 32 (ch: 216 = (120 + 96))

        bn_2 = BatchNormalization(axis=self.concat_axis, center=True)(denseblk_4)
        relu_2 = Activation('relu')(bn_2)
        transit_4 = Convolution3D(filters=192, kernel_size=(3, 3, 3), strides=(1, 1, 1), padding='same',
                                  use_bias=True, kernel_initializer='he_normal',
                                  kernel_regularizer=None)(relu_2)
        transit_4_2up = subpixel_2_3D_wChannel(24)(transit_4)   # 32 x 64 x 64 (ch: 24)

        conv_0_transit_4_2up = Concatenate()([conv_0, transit_4_2up])   # 32 x 64 x 64 (ch: 30)

        label_0 = self.last_label_layer_for_DenseNetv2(conv_0_transit_4_2up)
        label_1 = self.last_label_layer_for_DenseNetv2(conv_0_transit_4_2up)
        label_2 = self.last_label_layer_for_DenseNetv2(conv_0_transit_4_2up)
        label_3 = self.last_label_layer_for_DenseNetv2(conv_0_transit_4_2up)
        label_4 = self.last_label_layer_for_DenseNetv2(conv_0_transit_4_2up)
        label = Concatenate()([label_0, label_1, label_2, label_3, label_4])
        # label = Concatenate()([label_0, label_0, label_0, label_0, label_0])

        return label

    def DenseNet_v3(self, input):
        growth_k = 8
        # growth_k = 4
        theta = 0.5
        conv_0 = Convolution3D(filters=growth_k * 2, kernel_size=(7, 7, 7), strides=(2, 2, 2), padding='same',
                               use_bias=True, kernel_initializer='he_normal',
                               kernel_regularizer=None)(input)  # 32 x 64 x 64 (ch: 16)

        nb_layers = 12
        denseblk_1 = self.dense_block(conv_0, nb_layers=nb_layers, growth_k=growth_k)
        transit_1 = self.transition_layer(denseblk_1, theta=theta)
        bsize, zz, yy, xx, c = transit_1.get_shape().as_list()
        transit_1 = Convolution3D(filters=c, kernel_size=(3, 3, 3), strides=(2, 2, 2), padding='same',
                                  use_bias=True, kernel_initializer='he_normal',
                                  kernel_regularizer=None)(transit_1)  # 16 x 32 x 32 (ch: 56 = (16 + 96)/2)

        nb_layers = 24
        denseblk_2 = self.dense_block(transit_1, nb_layers=nb_layers, growth_k=growth_k)
        transit_2 = self.transition_layer(denseblk_2, theta=theta)
        bsize, zz, yy, xx, c = transit_2.get_shape().as_list()
        transit_2 = Convolution3D(filters=c, kernel_size=(3, 3, 3), strides=(2, 2, 2), padding='same',
                                  use_bias=True, kernel_initializer='he_normal',
                                  kernel_regularizer=None)(transit_2)  # 8 x 16 x 16 (ch: 124 = (56 + 24*8)/2

        nb_layers = 18
        denseblk_3 = self.dense_block(transit_2, nb_layers=nb_layers,
                                      growth_k=growth_k)  # 8 x 16 x 16 (ch: 268: 124 + 18*8)
        bn_1 = BatchNormalization(axis=self.concat_axis, center=True)(denseblk_3)
        relu_1 = Activation('relu')(bn_1)
        transit_3 = Convolution3D(filters=512, kernel_size=(3, 3, 3), strides=(1, 1, 1), padding='same',
                                  use_bias=True, kernel_initializer='he_normal',
                                  kernel_regularizer=None)(relu_1)  # 8 x 16 x 16 (ch: 124 = (56 + 24*8)
        transit_3_2up = subpixel_2_3D_wChannel(64)(transit_3)

        transit_1_transit_3_2up = Concatenate()([transit_1, transit_3_2up])  # 16 x 32 x 32 (ch: 120)
        nb_layers = 12
        denseblk_4 = self.dense_block(transit_1_transit_3_2up, nb_layers=nb_layers,
                                      growth_k=growth_k)  # 16 x 32 x 32 (ch: 216 = (120 + 96))

        label_0 = self.last_label_layer_for_DenseNetv3(denseblk_4)
        label_1 = self.last_label_layer_for_DenseNetv3(denseblk_4)
        label_2 = self.last_label_layer_for_DenseNetv3(denseblk_4)
        label_3 = self.last_label_layer_for_DenseNetv3(denseblk_4)
        label_4 = self.last_label_layer_for_DenseNetv3(denseblk_4)
        label = Concatenate()([label_0, label_1, label_2, label_3, label_4])

        return label

    def SimpleUnetwBatchnorm_ModLastDec(self, input):
        num_filters = 32
        x = Convolution2D(num_filters, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(input)
        x = BatchNormalization(center=True)(x)
        x = x_lev_0 = Convolution2D(num_filters, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)

        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters, 3, strides=(2, 2), padding='same', use_bias=True,
                          kernel_initializer='he_normal')(x)
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 2, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)
        x = x_lev_1 = Convolution2D(num_filters * 2, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(
            x)

        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 2, 3, strides=(2, 2), padding='same', use_bias=True,
                          kernel_initializer='he_normal')(x)
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 4, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)
        x = x_lev_2 = Convolution2D(num_filters * 4, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(
            x)

        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 4, 3, strides=(2, 2), padding='same', use_bias=True,
                          kernel_initializer='he_normal')(x)
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 8, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)
        x = x_lev_3 = Convolution2D(num_filters * 8, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(
            x)

        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 8, 3, strides=(2, 2), padding='same', use_bias=True,
                          kernel_initializer='he_normal')(x)
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 16, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 16, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)

        x = Conv2DTranspose(num_filters * 8, 3, strides=(2, 2), padding='same', use_bias=True,
                            kernel_initializer='he_normal')(x)

        x = Concatenate()([x, x_lev_3])
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 8, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 8, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)

        x = Conv2DTranspose(num_filters * 4, 3, strides=(2, 2), padding='same', use_bias=True,
                            kernel_initializer='he_normal')(x)

        x = Concatenate()([x, x_lev_2])
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 4, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 4, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)

        x = Conv2DTranspose(num_filters * 2, 3, strides=(2, 2), padding='same', use_bias=True,
                            kernel_initializer='he_normal')(x)

        x = Concatenate()([x, x_lev_1])
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 2, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 2, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)

        x = Conv2DTranspose(num_filters, 3, strides=(2, 2), padding='same', use_bias=True,
                            kernel_initializer='he_normal')(x)

        x = Concatenate()([x, x_lev_0])
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(30, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)
        x = BatchNormalization(center=True)(x)

        bsize, a, b, c = x.get_shape().as_list()
        bsize = tf.shape(x)[0]
        #################               FAILED
        x_label_0 = keras.backend.slice(x, [0, 0, 0, 0], [bsize, a, b, 6])
        # x_label_1 = keras.backend.slice(x, [0, 0, 0, 6], [-1, -1, -1, 6])
        # x_label_2 = keras.backend.slice(x, [0, 0, 0, 12], [-1, -1, -1, 6])
        # x_label_3 = keras.backend.slice(x, [0, 0, 0, 18], [-1, -1, -1, 6])
        # x_label_4 = keras.backend.slice(x, [0, 0, 0, 24], [-1, -1, -1, 6])
        # x_label_0 = Lambda(slicechannel_lamda)(x, 0, 6)
        # x_label_1 = Lambda(slicechannel_lamda)(x, 6, 12)
        # x_label_2 = Lambda(slicechannel_lamda)(x, 12, 18)
        # x_label_3 = Lambda(slicechannel_lamda)(x, 18, 24)
        # x_label_4 = Lambda(slicechannel_lamda)(x, 24, 30)
        # x_label_0 = x[:, :, :, 0:6]
        # x_label_1 = x[:, :, :, 6:12]
        # x_label_2 = x[:, :, :, 12:18]
        # x_label_3 = x[:, :, :, 18:24]
        # x_label_4 = x[:, :, :, 24:30]
        x_label_0 = Convolution2D(1, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x_label_0)
        x_label_1 = Convolution2D(1, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x_label_0)
        x_label_2 = Convolution2D(1, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x_label_0)
        x_label_3 = Convolution2D(1, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x_label_0)
        x_label_4 = Convolution2D(1, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x_label_0)

        # x = Convolution2D(5, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)
        x = Concatenate()([x_label_0, x_label_1, x_label_2, x_label_3, x_label_4])
        x = Softmax()(x)

        return x

    def SimpleUnetwBatchnorm(self, input):
        num_filters = 8
        x = Convolution2D(num_filters, 3, padding='same', use_bias=False, kernel_initializer='he_normal')(input)
        x = BatchNormalization(center=True)(x)
        x = x_lev_0 = Convolution2D(num_filters, 3, padding='same', use_bias=False, kernel_initializer='he_normal')(x)

        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters, 3, strides=(2, 2), padding='same', use_bias=False,
                          kernel_initializer='he_normal')(x)
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 2, 3, padding='same', use_bias=False, kernel_initializer='he_normal')(x)
        x = x_lev_1 = Convolution2D(num_filters * 2, 3, padding='same', use_bias=False, kernel_initializer='he_normal')(
            x)

        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 2, 3, strides=(2, 2), padding='same', use_bias=False,
                          kernel_initializer='he_normal')(x)
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 4, 3, padding='same', use_bias=False, kernel_initializer='he_normal')(x)
        x = x_lev_2 = Convolution2D(num_filters * 4, 3, padding='same', use_bias=False, kernel_initializer='he_normal')(
            x)

        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 4, 3, strides=(2, 2), padding='same', use_bias=False,
                          kernel_initializer='he_normal')(x)
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 8, 3, padding='same', use_bias=False, kernel_initializer='he_normal')(x)
        x = x_lev_3 = Convolution2D(num_filters * 8, 3, padding='same', use_bias=False, kernel_initializer='he_normal')(
            x)

        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 8, 3, strides=(2, 2), padding='same', use_bias=False,
                          kernel_initializer='he_normal')(x)
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 16, 3, padding='same', use_bias=False, kernel_initializer='he_normal')(x)
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 16, 3, padding='same', use_bias=False, kernel_initializer='he_normal')(x)

        x = Conv2DTranspose(num_filters * 8, 3, strides=(2, 2), padding='same', use_bias=False,
                            kernel_initializer='he_normal')(x)

        x = Concatenate()([x, x_lev_3])
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 8, 3, padding='same', use_bias=False, kernel_initializer='he_normal')(x)
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 8, 3, padding='same', use_bias=False, kernel_initializer='he_normal')(x)

        x = Conv2DTranspose(num_filters * 4, 3, strides=(2, 2), padding='same', use_bias=False,
                            kernel_initializer='he_normal')(x)

        x = Concatenate()([x, x_lev_2])
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 4, 3, padding='same', use_bias=False, kernel_initializer='he_normal')(x)
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 4, 3, padding='same', use_bias=False, kernel_initializer='he_normal')(x)

        x = Conv2DTranspose(num_filters * 2, 3, strides=(2, 2), padding='same', use_bias=False,
                            kernel_initializer='he_normal')(x)

        x = Concatenate()([x, x_lev_1])
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 2, 3, padding='same', use_bias=False, kernel_initializer='he_normal')(x)
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters * 2, 3, padding='same', use_bias=False, kernel_initializer='he_normal')(x)

        x = Conv2DTranspose(num_filters, 3, strides=(2, 2), padding='same', use_bias=False,
                            kernel_initializer='he_normal')(x)

        x = Concatenate()([x, x_lev_0])
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters, 3, padding='same', use_bias=False, kernel_initializer='he_normal')(x)
        x = BatchNormalization(center=True)(x)
        x = Convolution2D(num_filters, 3, padding='same', use_bias=False, kernel_initializer='he_normal')(x)

        x = BatchNormalization(center=True)(x)
        x = Convolution2D(5, 3, padding='same', use_bias=False, kernel_initializer='he_normal')(x)

        return x

    def SimpleUnet(self, input):
        num_filters = 8
        x = Convolution2D(num_filters, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(input)
        x = x_lev_0 = Convolution2D(num_filters, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)

        x = Convolution2D(num_filters, 3, strides=(2, 2), padding='same', use_bias=True,
                          kernel_initializer='he_normal')(x)
        x = Convolution2D(num_filters * 2, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)
        x = x_lev_1 = Convolution2D(num_filters * 2, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(
            x)

        x = Convolution2D(num_filters * 2, 3, strides=(2, 2), padding='same', use_bias=True,
                          kernel_initializer='he_normal')(x)
        x = Convolution2D(num_filters * 4, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)
        x = x_lev_2 = Convolution2D(num_filters * 4, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(
            x)

        x = Convolution2D(num_filters * 4, 3, strides=(2, 2), padding='same', use_bias=True,
                          kernel_initializer='he_normal')(x)
        x = Convolution2D(num_filters * 8, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)
        x = x_lev_3 = Convolution2D(num_filters * 8, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(
            x)

        x = Convolution2D(num_filters * 8, 3, strides=(2, 2), padding='same', use_bias=True,
                          kernel_initializer='he_normal')(x)
        x = Convolution2D(num_filters * 16, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)
        x = Convolution2D(num_filters * 16, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)

        x = Conv2DTranspose(num_filters * 8, 3, strides=(2, 2), padding='same', use_bias=True,
                            kernel_initializer='he_normal')(x)

        x = Concatenate()([x, x_lev_3])
        x = Convolution2D(num_filters * 8, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)
        x = Convolution2D(num_filters * 8, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)

        x = Conv2DTranspose(num_filters * 4, 3, strides=(2, 2), padding='same', use_bias=True,
                            kernel_initializer='he_normal')(x)

        x = Concatenate()([x, x_lev_2])
        x = Convolution2D(num_filters * 4, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)
        x = Convolution2D(num_filters * 4, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)

        x = Conv2DTranspose(num_filters * 2, 3, strides=(2, 2), padding='same', use_bias=True,
                            kernel_initializer='he_normal')(x)

        x = Concatenate()([x, x_lev_1])
        x = Convolution2D(num_filters * 2, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)
        x = Convolution2D(num_filters * 2, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)

        x = Conv2DTranspose(num_filters, 3, strides=(2, 2), padding='same', use_bias=True,
                            kernel_initializer='he_normal')(x)

        x = Concatenate()([x, x_lev_0])
        x = Convolution2D(num_filters, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)
        x = Convolution2D(num_filters, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)

        x = Convolution2D(5, 3, padding='same', use_bias=True, kernel_initializer='he_normal')(x)
        x = Softmax()(x)

        return x
