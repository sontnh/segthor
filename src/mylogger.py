from src.util_args import args
import logging
from pathlib import Path
import os
from datetime import datetime


def create_training_logger(log_dir=os.path.join(args.model_out_dir, 'pythonlog')):
    logger = logging.getLogger(__name__)
    now = datetime.now()
    streamHandler = logging.StreamHandler()
    path_log_dir = Path(log_dir)
    path_log_dir.mkdir(parents=True, exist_ok=True)
    fn_log = os.path.join(log_dir,
                          __name__ + '_%s-%s-%s_%s_%s.log' % (now.year, now.month, now.day, now.hour, now.minute))
    fileHandler = logging.FileHandler(fn_log)
    formatter = logging.Formatter('[%(levelname)s|%(filename)s:%(lineno)s] %(asctime)s > %(message)s')
    streamHandler.setFormatter(formatter)
    fileHandler.setFormatter(formatter)

    logger.addHandler(streamHandler)
    logger.addHandler(fileHandler)
    logger.setLevel(logging.DEBUG)
    logger.setLevel(logging.INFO)

    return logger

def create_eval_logger(log_dir=os.path.join(args.eval_predict_dir, 'pythonlog')):
    logger = logging.getLogger(__name__)
    now = datetime.now()
    streamHandler = logging.StreamHandler()
    path_log_dir = Path(log_dir)
    path_log_dir.mkdir(parents=True, exist_ok=True)
    fn_log = os.path.join(log_dir,
                          __name__ + '_%s-%s-%s_%s_%s.log' % (now.year, now.month, now.day, now.hour, now.minute))
    fileHandler = logging.FileHandler(fn_log)
    formatter = logging.Formatter('[%(levelname)s|%(filename)s:%(lineno)s] %(asctime)s > %(message)s')
    streamHandler.setFormatter(formatter)
    fileHandler.setFormatter(formatter)

    logger.addHandler(streamHandler)
    logger.addHandler(fileHandler)
    logger.setLevel(logging.DEBUG)
    logger.setLevel(logging.INFO)

    return logger


training_logger = create_training_logger()
eval_logger = create_eval_logger()
