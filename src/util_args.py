import argparse
import io
import os

import yaml
from pathlib import Path

DIR_CONFIG = Path(os.path.abspath(__file__)).parent
FN_CONFIG = os.path.join(DIR_CONFIG, 'config.yml')


def execute_args():
    args = parse_args(create_parser(is_default=not os.path.isfile(FN_CONFIG)))
    return args


def create_parser(is_default=False):
    parser = argparse.ArgumentParser()

    parser.add_argument('--dev', type=int, default=0 if is_default else None, help='gpu number to be assigned')
    parser.add_argument('--network', type=str, default='DenseNet_v1' if is_default else None)

    training_parser = parser.add_argument_group('training_args', 'Arguments for training')
    training_parser.add_argument('--seed', type=int, default=0 if is_default else None, help='Random seed')
    training_parser.add_argument('--dir_training_data', type=str,
                                 default='\\\\data5\\ImageData\\MachineLearning\\Challenge_SegTHOR\\train_mha_resampled\\patch3D_v2_h5' if is_default else None,
                                 help='directory for training')
    training_parser.add_argument('--dir_test_data', type=str,
                                 default='\\\\data5\\ImageData\\MachineLearning\\Challenge_SegTHOR\\train_mha_resampled\\patch3D_v2_h5' if is_default else None,
                                 help='directory for test')
    training_parser.add_argument('--model_out_dir', type=str,
                                 default='\\\\data5\\Workspace\\haison\\Challenge_SegTHOR\\DenseNet' if is_default else None)
    training_parser.add_argument('--chkpnt', type=str, default='None' if is_default else None)
    training_parser.add_argument('--optimizer', type=str, default='AdamWAccumulate' if is_default else None)
    training_parser.add_argument('--num_accum', type=int, default=16 if is_default else None)
    training_parser.add_argument('--total_epochs', type=int, default=500 if is_default else None)
    training_parser.add_argument('--epoch_size', type=int, default=2048 if is_default else None)
    training_parser.add_argument('--minibatch_size', type=int, default=2 if is_default else None)
    training_parser.add_argument('--learning_rate_base', type=float, default=5e-4 if is_default else None)
    training_parser.add_argument('--weight_decay', type=float, default=1e-5 if is_default else None)
    training_parser.add_argument('--training_db_name', type=str, default='SegTHOR' if is_default else None,
                                 help='trainin DB name')
    training_parser.add_argument('--clr_mode', type=str, default='None' if is_default else None,
                                 help='If you want to turn on cyclic learning rate (mode: None, triangular, triangular2, exp_range')
    training_parser.add_argument('--learning_rate_max_clr', type=float, default=5e-3 if is_default else None)
    training_parser.add_argument('--step_clr', type=float, default=25600. if is_default else None)
    training_parser.add_argument('--further_info', type=str, default='experiment description e.g. purpose, new things')

    training_parser.add_argument('--fn_pattern_test_input', type=str,
                                 default='Patient_[0][1-3]_[0-9][0-9][0-9][0-9]_CT.*' if is_default else None)
    training_parser.add_argument('--fn_pattern_test_label', type=str,
                                 default='Patient_[0][1-3]_[0-9][0-9][0-9][0-9]_GT.*' if is_default else None)

    training_parser.add_argument('--fn_pattern_valid_input_0', type=str,
                                 default='label_0/Patient_[0][3-9]_[0-9][0-9][0-9][0-9]_CT.*' if is_default else None)
    training_parser.add_argument('--fn_pattern_valid_label_0', type=str,
                                 default='label_0/Patient_[0][3-9]_[0-9][0-9][0-9][0-9]_GT.*' if is_default else None)

    training_parser.add_argument('--fn_pattern_valid_input_1', type=str,
                                 default='label_1/Patient_[0][3-9]_[0-9][0-9][0-9][0-9]_CT.*' if is_default else None)
    training_parser.add_argument('--fn_pattern_valid_label_1', type=str,
                                 default='label_1/Patient_[0][3-9]_[0-9][0-9][0-9][0-9]_GT.*' if is_default else None)

    training_parser.add_argument('--fn_pattern_valid_input_2', type=str,
                                 default='label_2/Patient_[0][3-9]_[0-9][0-9][0-9][0-9]_CT.*' if is_default else None)
    training_parser.add_argument('--fn_pattern_valid_label_2', type=str,
                                 default='label_2/Patient_[0][3-9]_[0-9][0-9][0-9][0-9]_GT.*' if is_default else None)

    training_parser.add_argument('--fn_pattern_valid_input_3', type=str,
                                 default='label_3/Patient_[0][3-9]_[0-9][0-9][0-9][0-9]_CT.*' if is_default else None)
    training_parser.add_argument('--fn_pattern_valid_label_3', type=str,
                                 default='label_3/Patient_[0][3-9]_[0-9][0-9][0-9][0-9]_GT.*' if is_default else None)

    training_parser.add_argument('--fn_pattern_valid_input_4', type=str,
                                 default='label_4/Patient_[0][3-9]_[0-9][0-9][0-9][0-9]_CT.*' if is_default else None)
    training_parser.add_argument('--fn_pattern_valid_label_4', type=str,
                                 default='label_4/Patient_[0][3-9]_[0-9][0-9][0-9][0-9]_GT.*' if is_default else None)

    training_parser.add_argument('--fn_pattern_train_input_0', type=str,
                                 default='label_0/Patient_[1-9][0-9]_[0-9][0-9][0-9][0-9]_CT.*' if is_default else None)
    training_parser.add_argument('--fn_pattern_train_label_0', type=str,
                                 default='label_0/Patient_[1-9][0-9]_[0-9][0-9][0-9][0-9]_GT.*' if is_default else None)

    training_parser.add_argument('--fn_pattern_train_input_1', type=str,
                                 default='label_1/Patient_[1-9][0-9]_[0-9][0-9][0-9][0-9]_CT.*' if is_default else None)
    training_parser.add_argument('--fn_pattern_train_label_1', type=str,
                                 default='label_1/Patient_[1-9][0-9]_[0-9][0-9][0-9][0-9]_GT.*' if is_default else None)

    training_parser.add_argument('--fn_pattern_train_input_2', type=str,
                                 default='label_2/Patient_[1-9][0-9]_[0-9][0-9][0-9][0-9]_CT.*' if is_default else None)
    training_parser.add_argument('--fn_pattern_train_label_2', type=str,
                                 default='label_2/Patient_[1-9][0-9]_[0-9][0-9][0-9][0-9]_GT.*' if is_default else None)

    training_parser.add_argument('--fn_pattern_train_input_3', type=str,
                                 default='label_3/Patient_[1-9][0-9]_[0-9][0-9][0-9][0-9]_CT.*' if is_default else None)
    training_parser.add_argument('--fn_pattern_train_label_3', type=str,
                                 default='label_3/Patient_[1-9][0-9]_[0-9][0-9][0-9][0-9]_GT.*' if is_default else None)

    training_parser.add_argument('--fn_pattern_train_input_4', type=str,
                                 default='label_4/Patient_[1-9][0-9]_[0-9][0-9][0-9][0-9]_CT.*' if is_default else None)
    training_parser.add_argument('--fn_pattern_train_label_4', type=str,
                                 default='label_4/Patient_[1-9][0-9]_[0-9][0-9][0-9][0-9]_GT.*' if is_default else None)

    evaluation_parser = parser.add_argument_group('evaluation_args', 'Arguments for evaluation')
    evaluation_parser.add_argument('--eval_model_dir', type=str,
                                   default='\\\\data5\\Workspace\\haison\\Challenge_SegTHOR\\DenseNet_seed_1101')
    evaluation_parser.add_argument('--eval_model_fn', type=str,
                                   default='ckpt-329-valDice_0_0.993-valDice_1_0.714-valDice_2_0.925-valDice_3_0.742-valDice_4_0.784.h5')
    evaluation_parser.add_argument('--eval_predict_dir', type=str,
                                   default='\\\\data5\\Workspace\\haison\\Challenge_SegTHOR\\eval\\DenseNet_seed_1101')
    # evaluation_parser.add_argument('--LUNA_dir', type=str, default=None)
    # evaluation_parser.add_argument('--test_subset', type=int, default=None)
    # evaluation_parser.add_argument('--test_idx', type=int, default=None)

    return parser


def parse_args(parser):
    args = parser.parse_args()
    args_dict = args.__dict__
    if os.path.isfile(FN_CONFIG):
        with open(FN_CONFIG, 'r') as fid:
            data = yaml.load(fid)
        for key, value in data.items():
            args_dict[key] = value if args_dict[key] is None else args_dict[key]
    else:
        with io.open(FN_CONFIG, 'w', encoding='utf8') as fid:
            yaml.dump(args_dict, fid, default_flow_style=False, allow_unicode=True)

    args_dict['model_out_dir'] = os.path.join(args.model_out_dir + '_seed_%d' % args.seed)

    return args


args = execute_args()
