from keras.layers import Input, Softmax
import random
import keras.losses
import keras
from keras.callbacks import ModelCheckpoint, TensorBoard
import os
import numpy as np
from src.Model_SegthorNet import SegthorNet
from src.AdamAccumulate import AdamWAccumulate
from pathlib import Path
from keras.utils.data_utils import GeneratorEnqueuer

import tensorflow as tf
from keras import backend as K
#from prefetch_generator import background

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
session = tf.Session(config=config)

from src.mylogger import training_logger


def focal_loss(pred, label, gamma=2):
    # - label * (1 - p)^gamma * log(p)
    return - label * tf.pow(1 - pred, gamma) * tf.log(pred + 1e-07)


def cross_ent(pred, label):
    return - label * tf.log(pred + 1e-07)


def binary_focal_loss(pred, label, gamma=2):
    pos = focal_loss(pred, label, gamma=gamma)
    neg = focal_loss(1 - pred, 1 - label, gamma=gamma)

    return (pos + neg)


def focal_loss_mod_each_channel(pred, label, gamma=2):
    weight_neg = K.sum(tf.to_float(tf.equal(label, 1)))
    weight_pos = K.sum(tf.to_float(tf.equal(label, 0)))

    total = K.sum(weight_pos + weight_neg)
    weight_pos /= total
    weight_neg /= total
    pos = focal_loss(pred, label, gamma=gamma) * weight_pos
    neg = focal_loss(1 - pred, 1 - label, gamma=gamma) * weight_neg
    return pos, neg


def mix_loss_mod_each_channel(pred, label, gamma=2):
    weight_neg = K.sum(tf.to_float(tf.equal(label, 1)))
    weight_pos = K.sum(tf.to_float(tf.equal(label, 0)))

    total = K.sum(weight_pos + weight_neg)
    weight_pos /= total
    weight_neg /= total
    pos = cross_ent(pred, label) * weight_pos
    neg = focal_loss(1 - pred, 1 - label, gamma=gamma) * weight_neg

    return pos, neg


def binary_focal_loss(pred, label, gamma=2):
    # ratio for transaxial:
    # background (0) : 0.9832272689846752,
    # label(1) : 0.0007186902124976425,
    # label(2) : 0.01242931261794914,
    # label(3) : 0.0005432480270124222,
    # label(4) : 0.0030814801578656114)
    # pos_0 = focal_loss(pred[:, :, :, 0], label[:, :, :, 0], gamma=gamma) / 1000

    pred = Softmax()(pred)
    _epsilon = 1e-07
    pred = tf.clip_by_value(pred, _epsilon, 1 - _epsilon)

    weights = [0.00055, 0.75589, 0.04371, 1.0, 0.17629]  # original weight
    # weights = [0.02, 0.75589, 0.04371, 1.0, 0.17629]
    weights = [1., 1., 1., 1., 1.]

    pos_0, neg_0 = focal_loss_mod_each_channel(pred=pred[:, :, :, :, 0], label=label[:, :, :, :, 0], gamma=gamma)
    pos_1, neg_1 = focal_loss_mod_each_channel(pred=pred[:, :, :, :, 1], label=label[:, :, :, :, 1], gamma=gamma)
    pos_2, neg_2 = focal_loss_mod_each_channel(pred=pred[:, :, :, :, 2], label=label[:, :, :, :, 2], gamma=gamma)
    pos_3, neg_3 = focal_loss_mod_each_channel(pred=pred[:, :, :, :, 3], label=label[:, :, :, :, 3], gamma=gamma)
    pos_4, neg_4 = focal_loss_mod_each_channel(pred=pred[:, :, :, :, 4], label=label[:, :, :, :, 4], gamma=gamma)
    # pos_0 *= weights[0]
    # pos_1 *= weights[1]
    # pos_2 *= weights[2]
    # pos_3 *= weights[3]
    # pos_4 *= weights[4]
    #
    # neg_0 *= weights[0]
    # neg_1 *= weights[1]
    # neg_2 *= weights[2]
    # neg_3 *= weights[3]
    # neg_4 *= weights[4]

    # pos_0 = focal_loss(pred[:, :, :, :, 0], label[:, :, :, :, 0], gamma=gamma) * weights[0]
    # neg_0 = focal_loss(1 - pred[:, :, :, :, 0], 1 - label[:, :, :, :, 0], gamma=gamma) * weights[0]
    #
    # pos_1 = focal_loss(pred[:, :, :, :, 1], label[:, :, :, :, 1], gamma=gamma) * weights[1]
    # neg_1 = focal_loss(1 - pred[:, :, :, :, 1], 1 - label[:, :, :, :, 1], gamma=gamma) * weights[1]
    #
    # pos_2 = focal_loss(pred[:, :, :, :, 2], label[:, :, :, :, 2], gamma=gamma) * weights[2]
    # neg_2 = focal_loss(1 - pred[:, :, :, :, 2], 1 - label[:, :, :, :, 2], gamma=gamma) * weights[2]
    #
    # pos_3 = focal_loss(pred[:, :, :, :, 3], label[:, :, :, :, 3], gamma=gamma) * weights[3]
    # neg_3 = focal_loss(1 - pred[:, :, :, :, 3], 1 - label[:, :, :, :, 3], gamma=gamma) * weights[3]
    #
    # pos_4 = focal_loss(pred[:, :, :, :, 4], label[:, :, :, :, 4], gamma=gamma) * weights[4]
    # neg_4 = focal_loss(1 - pred[:, :, :, :, 4], 1 - label[:, :, :, :, 4], gamma=gamma) * weights[4]

    # pos_0 = cross_ent(pred[:, :, :, 0], label[:, :, :, 0]) * weights[0]
    # neg_0 = cross_ent(1 - pred[:, :, :, 0], 1 - label[:, :, :, 0]) * weights[0]
    # pos_1 = cross_ent(pred[:, :, :, 1], label[:, :, :, 1]) * weights[1]
    # neg_1 = cross_ent(1 - pred[:, :, :, 1], 1 - label[:, :, :, 1]) * weights[1]
    # pos_2 = cross_ent(pred[:, :, :, 2], label[:, :, :, 2]) * weights[2]
    # neg_2 = cross_ent(1 - pred[:, :, :, 2], 1 - label[:, :, :, 2]) * weights[2]
    # pos_3 = cross_ent(pred[:, :, :, 3], label[:, :, :, 3]) * weights[3]
    # neg_3 = cross_ent(1 - pred[:, :, :, 3], 1 - label[:, :, :, 3]) * weights[3]
    # pos_4 = cross_ent(pred[:, :, :, 4], label[:, :, :, 4]) * weights[4]
    # neg_4 = cross_ent(1 - pred[:, :, :, 4], 1 - label[:, :, :, 4]) * weights[4]

    return pos_1 + pos_2 + pos_3 + pos_4 + neg_1 + neg_2 + neg_3 + neg_4 + pos_0 + neg_0


def classifier_loss(y_true, y_pred):
    classification_loss = binary_focal_loss(pred=y_pred[:, :, :, :, :], label=y_true[:, :, :, :, :])
    return classification_loss


def DiceCoeff(y_true, y_pred, target=0):
    y_pred_argmax = K.argmax(y_pred)
    y_pred_target_tmp = K.equal(y_pred_argmax, target)
    # y_pred_target = K.squeeze(K.cast_to_floatx(y_pred_target_tmp))
    y_pred_target = K.cast(y_pred_target_tmp, np.float32)
    # y_true_target = K.floatx(K.greater(y_true[:, :, target], 0))
    y_true_target = y_true[:, :, :, :, target]
    intersection = K.sum(y_true_target * y_pred_target)
    return (2. * intersection) / (K.sum(y_true_target) + K.sum(y_pred_target) + 1e-7)


def DiceCoeff_0(y_true, y_pred):
    return DiceCoeff(y_true, y_pred, 0)


def DiceCoeff_1(y_true, y_pred):
    return DiceCoeff(y_true, y_pred, 1)


def DiceCoeff_2(y_true, y_pred):
    return DiceCoeff(y_true, y_pred, 2)


def DiceCoeff_3(y_true, y_pred):
    return DiceCoeff(y_true, y_pred, 3)


def DiceCoeff_4(y_true, y_pred):
    return DiceCoeff(y_true, y_pred, 4)


def get_total_validdata(reader_valid_list):
    reader_valid_size_each = 20

    mb_input = None
    mb_label = None

    for reader_valid in reader_valid_list:
        mb_input_tmp, mb_label_tmp = reader_valid.next_minibatch(reader_valid_size_each)
        if mb_input is None:
            mb_input = mb_input_tmp
            mb_label = mb_label_tmp
        else:
            mb_input = np.concatenate((mb_input, mb_input_tmp))
            mb_label = np.concatenate((mb_label, mb_label_tmp))

    # mb_input_tmp = np.squeeze(np.array(mb_input[0])).astype(np.float32)
    # mb_input_tmp = np.transpose(mb_input_tmp, axes=[2, 0, 1])
    # mb_input_tmp.tofile('mb_input_tmp_%dx%dx%d.raw' % tuple(reversed(np.shape(mb_input_tmp))))
    # mb_label_tmp = np.squeeze(np.array(mb_label[0])).astype(np.float32)
    # mb_label_tmp = np.transpose(mb_label_tmp, axes=[2, 0, 1])
    # mb_label_tmp.tofile('mb_label_tmp_%dx%dx%d.raw' % tuple(reversed(np.shape(mb_label_tmp))))
    # input('get_total_validdata...')
    return mb_input, mb_label


# @background(max_prefetch=16)
def generator_train(reader_train_list, batch_size=2):
    while True:
        mb_input = None
        mb_label = None
        for idx in range(batch_size):
            label_idx = random.randint(0, len(reader_train_list) - 1)
            mb_input_tmp, mb_label_tmp = reader_train_list[label_idx].next_minibatch(1)
            if mb_input is None:
                mb_input = mb_input_tmp
                mb_label = mb_label_tmp
            else:
                mb_input = np.concatenate((mb_input, mb_input_tmp), axis=0)
                mb_label = np.concatenate((mb_label, mb_label_tmp), axis=0)

        yield mb_input, mb_label


def train_by_keras(reader_train_list, reader_valid_list, epoch_size, total_epochs, minibatch_size, model_dir,
                   tensorboard_logdir,
                   input_dim, restore_fn_dnn=None, args=None):
    num_channels = 1
    zdim, ydim, xdim = input_dim

    input_var = Input(shape=(zdim, ydim, xdim, num_channels), name='Input', dtype=np.float32)
    model = SegthorNet(input_var, args.network).model
    if restore_fn_dnn is not None:
        model.load_weights(restore_fn_dnn)

    model.summary()
    init_lr = args.learning_rate_base
    weight_decay = args.weight_decay

    optimizer = AdamWAccumulate(lr=init_lr, accum_iters=args.num_accum, weight_decay=weight_decay, beta_1=0.997, beta_2=0.99997)
    training_logger.info('AdamWAccumulate optimizer (lr: %f, accum_iters: %d, weight_decay: %f)' % (
        init_lr, args.num_accum, weight_decay))

    model.compile(optimizer=optimizer, loss=classifier_loss,
                  metrics=['acc', DiceCoeff_0, DiceCoeff_1, DiceCoeff_2, DiceCoeff_3, DiceCoeff_4])

    start_epoch = 0

    if restore_fn_dnn is not None:
        start_epoch = int(Path(restore_fn_dnn).stem.split('-')[1])
        training_logger.info('restored start_epoch: ' + str(start_epoch + 1))

    callbacks = []

    if model_dir is not None:
        weights_file = os.path.join(model_dir,
                                    'ckpt-{epoch:02d}-valDice_0_{val_DiceCoeff_0:.3f}-valDice_1_{val_DiceCoeff_1:.3f}-valDice_2_{val_DiceCoeff_2:.3f}-valDice_3_{val_DiceCoeff_3:.3f}-valDice_4_{val_DiceCoeff_4:.3f}.h5')
        model_checkpoint = ModelCheckpoint(weights_file, save_weights_only=True, verbose=1)
        callbacks.append(model_checkpoint)

    if tensorboard_logdir is not None:
        tensorboard = TensorBoard(tensorboard_logdir, histogram_freq=0, write_graph=True)
        callbacks.append(tensorboard)

    mb_input_valid, mb_label_valid = get_total_validdata(reader_valid_list=reader_valid_list)

    # model.fit_generator(generator=generator_train(reader_train=reader_train, batch_size=minibatch_size),
    #                     steps_per_epoch=epoch_size // minibatch_size, epochs=total_epochs,
    #                     callbacks=callbacks, validation_data=(mb_input_valid, mb_label_valid),
    #                     initial_epoch=start_epoch, max_queue_size=256)
    training_generator = DataGenerator(reader_train_list, minibatch_size)
    model.fit_generator(generator=training_generator,
                        steps_per_epoch=epoch_size // minibatch_size, epochs=total_epochs,
                        callbacks=callbacks, validation_data=(mb_input_valid, mb_label_valid),
                        initial_epoch=start_epoch, workers=4, max_queue_size=16)

    # mb_label_predict = model.predict(x=mb_input_valid)


class DataGenerator(keras.utils.Sequence):
    def __init__(self, reader_train_list, batch_size):
        self.reader_train_list = reader_train_list
        self.batch_size = batch_size

    def __len__(self):
        length = 0
        for reader_train in self.reader_train_list:
            length += reader_train.get_list_size()

        return length

    def __getitem__(self, item):
        # mb_input, mb_label = self.reader_train.next_minibatch(self.batch_size)
        # return mb_input, mb_label
        mb_input = None
        mb_label = None
        for idx in range(self.batch_size):
            label_idx = random.randint(0, len(self.reader_train_list) - 1)
            size_tmp = self.reader_train_list[label_idx].get_list_size()
            idx_rand = random.randint(0, size_tmp - 1)
            # mb_input_tmp, mb_label_tmp = self.reader_train_list[label_idx].next_minibatch(1)
            mb_input_tmp, mb_label_tmp = self.reader_train_list[label_idx].at(idx_rand)
            if mb_input is None:
                mb_input = mb_input_tmp
                mb_label = mb_label_tmp
            else:
                mb_input = np.concatenate((mb_input, mb_input_tmp), axis=0)
                mb_label = np.concatenate((mb_label, mb_label_tmp), axis=0)

        return mb_input, mb_label
