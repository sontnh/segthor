import os
import numpy as np
import nibabel as nib
import SimpleITK as itk
import csv

def main():
    """ main function to analze dataset """
    root_dir = "/mnt/Data4/ImageData/SegTHOR_challenge/train"
    pat_cnt = 40
    label_file = "GT.nii.gz"
    dic_label = {"Esophagus": int(1), "Heart": int(2), "Trachea": int(3), "Aorta": int(4)}
    label_cnt = len(dic_label)

    report_file = "report_%d_patients.csv" % (pat_cnt)
    out_dir = "/mnt/Data5/Workspace/haison/SegTHOR/training/analysis_dataset"
    if not os.path.exists(out_dir):
        os.makedirs(out_dir, exist_ok=True)

    report_filepath = os.path.join(out_dir, report_file)
    print( "report file : " + report_filepath)

    with open(report_filepath, 'w', newline='') as csvfile:
        report_writer = csv.writer(csvfile, delimiter=',')
        csv_header = ["volume_file", "label_file", "dimZ", "dimY", "dimX", "resZ", "resY", "resX"]
        for k in dic_label:
            csv_header += ["%s_%d" % ( k, dic_label[k] )]
        report_writer.writerow(csv_header)
      
        for i in range(1, pat_cnt + 1):
            pat_name = "Patient_%02d" % (i)
            print("> patient : " + pat_name)

            rel_path_vol = "%s/%s.nii.gz" % (pat_name, pat_name)
            rel_path_label = "%s/%s" % (pat_name, label_file)
            
            img_label = itk.ReadImage(os.path.join(root_dir, rel_path_label))
            spacing = list(reversed(img_label.GetSpacing()))
            print(spacing)

            img_label = itk.GetArrayFromImage(img_label)
            shape = list(np.shape(img_label))
            print(shape)

            row = [ rel_path_vol, rel_path_label ]
            row += shape
            row += spacing

            cnt_label = []

            for k in dic_label:
                cnt = np.count_nonzero(img_label == dic_label[k])
                print("> %s : %d" % (k, cnt))
                cnt_label += [cnt]
            
            row += cnt_label
            report_writer.writerow(row)

    
if __name__ is "__main__":
    main()