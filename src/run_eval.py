import numpy as np
import queue
import os

os.environ['CUDA_VISIBLE_DEVICES'] = '%d' % 1
from pathlib import Path
import itertools
import SimpleITK as sitk
from src.util_args import args, FN_CONFIG
from keras.layers import Input
from src.mylogger import eval_logger
import shutil


def configure_model(model_dir, model_fn, network_name, net_input_dim):
    import tensorflow as tf
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    session = tf.Session(config=config)

    num_channels = 1
    zdim, ydim, xdim = net_input_dim

    input_var = Input(shape=(zdim, ydim, xdim, num_channels), name='Input', dtype=np.float32)

    from src.Model_SegthorNet import SegthorNet
    model = SegthorNet(input_var, network_name=network_name).model

    restore_fn_dnn = os.path.join(model_dir, model_fn)
    if restore_fn_dnn is not None:
        model.load_weights(restore_fn_dnn)
    model.summary()

    return model


def evaluate_CT_vol(model, pid):
    # fn_CTvol_mha = '\\\\data5\\ImageData\\MachineLearning\\Challenge_SegTHOR\\train_mha_resampled\\Patient_09.mha'
    fn_CTvol_mha = '\\\\data4\\ImageData\\SegTHOR_challenge\\test\\Patient_%02d.nii.gz' % pid
    # fn_GTvol_mha = '\\\\data5\\ImageData\\MachineLearning\\Challenge_SegTHOR\\train_mha_resampled\\Patient_09_GT.mha'

    pname = Path(fn_CTvol_mha).stem
    norm_wind_upper = 2000
    norm_wind_lower = -1000
    net_input_dim = (64, 128, 128)
    stride_input_dim = (32, 64, 64)

    CTvol_img = sitk.ReadImage(fn_CTvol_mha)
    # GTvol_img = sitk.ReadImage(fn_GTvol_mha)
    CTvol_raw = np.array(sitk.GetArrayFromImage(CTvol_img)).astype(np.float32)
    # GTvol_raw = np.array(sitk.GetArrayFromImage(GTvol_img)).astype(np.float32)
    CTvol_shape = np.shape(CTvol_raw)

    CTvol_raw = (CTvol_raw - norm_wind_lower) / (norm_wind_upper - norm_wind_lower)
    CTvol_raw[np.where(CTvol_raw < 0)] = 0
    CTvol_raw[np.where(CTvol_raw > 1.)] = 1.

    xx_list = np.concatenate((
        np.arange(0, CTvol_shape[2] - net_input_dim[2], stride_input_dim[2]),
        np.array([CTvol_shape[2] - net_input_dim[2]])
    ))
    yy_list = np.concatenate((
        np.arange(0, CTvol_shape[1] - net_input_dim[1], stride_input_dim[1]),
        np.array([CTvol_shape[1] - net_input_dim[1]])
    ))
    zz_list = np.concatenate((
        np.arange(0, CTvol_shape[0] - net_input_dim[0], stride_input_dim[0]),
        np.array([CTvol_shape[0] - net_input_dim[0]])
    ))

    num_whole_process = len(xx_list) * len(yy_list) * len(zz_list)
    process_chk_idx = 0
    dupl_manager_vol = np.zeros(shape=np.shape(CTvol_raw))
    prediction_vol = np.zeros(shape=list(np.shape(CTvol_raw)) + [5])
    for zz, yy, xx in itertools.product(zz_list, yy_list, xx_list):
        if process_chk_idx % 50 == 0:
            eval_logger.info('Process: %d / %d' % (process_chk_idx + 1, num_whole_process))

        CTvol_raw_patch = CTvol_raw[zz:zz + net_input_dim[0], yy:yy + net_input_dim[1], xx:xx + net_input_dim[2]]
        CTvol_raw_patch = np.expand_dims(CTvol_raw_patch, axis=3)
        CTvol_raw_patch = np.expand_dims(CTvol_raw_patch, axis=0)
        prediction = np.squeeze(model.predict(x=CTvol_raw_patch))
        # eval_logger.info('prediction shape: ' + str(np.shape(prediction)))
        # prediction_vol[int(zz + net_input_dim[0] / 4):int(zz + net_input_dim[0] / 4 * 3),
        # int(yy + net_input_dim[1] / 4):int(yy + net_input_dim[1] / 4 * 3),
        # int(xx + net_input_dim[2] / 4):int(xx + net_input_dim[2] / 4 * 3), :] = \
        #     prediction[int(net_input_dim[0] / 4):int(net_input_dim[0] / 4 * 3),
        #     int(net_input_dim[1] / 4):int(net_input_dim[1] / 4 * 3),
        #     int(net_input_dim[2] / 4):int(net_input_dim[2] / 4 * 3), :]

        # dupl_manager_vol[int(zz + net_input_dim[0] / 4):int(zz + net_input_dim[0] / 4 * 3),
        # int(yy + net_input_dim[1] / 4):int(yy + net_input_dim[1] / 4 * 3),
        # int(xx + net_input_dim[2] / 4):int(xx + net_input_dim[2] / 4 * 3)] += 1

        if zz == zz_list[0]:
            z_start = 0
            z_end = int(net_input_dim[0])
        else:
            z_start = int(net_input_dim[0] / 4)
            z_end = net_input_dim[0]

        if yy == yy_list[0]:
            y_start = 0
            y_end = int(net_input_dim[1])
        else:
            y_start = int(net_input_dim[1] / 4)
            y_end = net_input_dim[1]

        if xx == xx_list[0]:
            x_start = 0
            x_end = int(net_input_dim[2])
        else:
            x_start = int(net_input_dim[2] / 4)
            x_end = net_input_dim[2]
        prediction_vol[zz + z_start:zz + z_end, yy + y_start:yy + y_end, xx + x_start:xx + x_end, :] = \
            prediction[z_start:z_end, y_start:y_end, x_start:x_end, :]

        dupl_manager_vol[zz + z_start:zz + z_end, yy + y_start:yy + y_end, xx + x_start:xx + x_end] += 1

        process_chk_idx += 1

    eval_predict_final = os.path.join(args.eval_predict_dir, args.eval_model_fn, pname)
    Path(eval_predict_final).mkdir(parents=True, exist_ok=True)

    # prediction_vol_tr = np.transpose(prediction_vol, [3, 0, 1, 2]).astype(np.float32)
    # fn_prediction_vol = os.path.join(eval_predict_final,
    #                                  'prediction_vol_%dx%dx%dx%d.raw' % tuple(reversed(prediction_vol_tr.shape)))
    # prediction_vol_tr.tofile(fn_prediction_vol)

    prediction_argmax = np.argmax(prediction_vol, axis=-1)
    fn_prediction_argmax = os.path.join(eval_predict_final,
                                        'prediction_argmax_%dx%dx%d.raw' % tuple(reversed(prediction_argmax.shape)))

    prediction_argmax.astype(np.int8).tofile(fn_prediction_argmax)
    fn_gt_topredictdir = os.path.join(eval_predict_final, '%s_GT.mha' % pname)
    # shutil.copy(fn_GTvol_mha, fn_gt_topredictdir)

    prediction_argmax_ccl = CCL(prediction_argmax.astype(np.uint8))
    fn_prediction_argmax_ccl = os.path.join(eval_predict_final,
                                            'prediction_argmax_ccl_%dx%dx%d.raw' % tuple(
                                                reversed(prediction_argmax_ccl.shape)))
    prediction_argmax_ccl.tofile(fn_prediction_argmax_ccl)


def CCL(vol):
    new_vol = np.zeros(np.shape(vol)).astype(np.uint8)
    for idx in range(1, 5):
        vol_binary = np.zeros(np.shape(vol)).astype(np.uint16)
        vol_binary[np.where(vol == idx)] = 1

        tmp_q_idx = 0
        target_ccl_cc = 0
        target_ccl_idx = 1
        while True:
            idcs_valid = np.where(vol_binary == 1)
            start_num_idcs_valid = len(idcs_valid[0])
            print('num idcs_valid : %d' % len(idcs_valid[0]))
            if len(idcs_valid[0]) == 0:
                break
            q_tmp = queue.Queue()
            point_tmp = (idcs_valid[0][0], idcs_valid[1][0], idcs_valid[2][0])
            q_tmp.put(point_tmp)
            while q_tmp.qsize():
                point_tmp = q_tmp.get()
                vol_binary[point_tmp] = 2 + tmp_q_idx
                for neighbor in get26n(vol, point_tmp):
                    if vol_binary[neighbor] == 1:
                        q_tmp.put(neighbor)
                        vol_binary[neighbor] = 2 + tmp_q_idx

            idcs_valid = np.where(vol_binary == 1)
            end_num_idcs_valid = len(idcs_valid[0])

            if target_ccl_cc < start_num_idcs_valid - end_num_idcs_valid:
                target_ccl_idx = 2 + tmp_q_idx
                target_ccl_cc = start_num_idcs_valid - end_num_idcs_valid

            tmp_q_idx += 1

        new_vol[np.where(vol_binary == target_ccl_idx)] = idx

    return new_vol


def get26n(vol, point):
    seed_z, seed_y, seed_x = point

    out = []

    shape_vol = np.shape(vol)

    for zz in range(-1, 2, 1):
        for yy in range(-1, 2, 1):
            for xx in range(-1, 2, 1):
                x = seed_x + xx
                y = seed_y + yy
                z = seed_z + zz

                x = min(max(x, 0), shape_vol[2] - 1)
                y = min(max(y, 0), shape_vol[1] - 1)
                z = min(max(z, 0), shape_vol[0] - 1)

                out.append((z, y, x))

    return out


def main():
    net_input_dim = (64, 128, 128)
    net_output_dim = (5, 64, 128, 128)
    model_dir = args.eval_model_dir
    model_fn = args.eval_model_fn
    network_name = args.network

    model = configure_model(model_dir=model_dir, model_fn=model_fn, network_name=network_name,
                            net_input_dim=net_input_dim)

    for pid in range(41, 61):
        evaluate_CT_vol(model, pid)


if __name__ == '__main__':
    os.environ['CUDA_VISIBLE_DEVICES'] = '%d' % args.dev
    main()
